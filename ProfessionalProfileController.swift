//
//  ProfessionalProfileController.swift
//  Paindown
//
//  Created by James Suske on 2018-08-31.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit

class ProfessionalProfileController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, ProfessionalReviewsDelegate {
    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var sendMessageButton: UIButton!
    
    var username = String()
    
    @IBOutlet var user: UILabel!
    
    @IBOutlet var location: UILabel!
    
    @IBOutlet var profileCollection: UICollectionView!
    
    var profileArray = Array<ProfessionalProfileClass>()
    
    var userArray = Dictionary<String, Any>()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.profileArray.append(ProfessionalProfileClass(title: "About", controller: "ProfessionalAbout"))
        self.profileArray.append(ProfessionalProfileClass(title: "Medical Information", controller: "ProfessionalMedicalInformation"))
        self.profileArray.append(ProfessionalProfileClass(title: "Reviews", controller: "ProfessionalReviews"))
        
        self.profileCollection.delegate = self
        self.profileCollection.dataSource = self
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = (profileImage.frame.height) / 2
        profileImage.clipsToBounds = true
        
        getProfessional(username: self.username){ result in
            
            self.userArray = result
            
            let imgUrl = URL(string:(result["image"] as! String))
            
            self.profileImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
            
            self.user.text = (result["username"] as! String)
            
            let city = ((result["location"] as! Dictionary<String, Any>)["city"] as! Dictionary<String, Any>)["name"] as! String
            let country = ((result["location"] as! Dictionary<String, Any>)["country"] as! Dictionary<String, Any>)["name"] as! String
            let region = ((result["location"] as! Dictionary<String, Any>)["region"] as! Dictionary<String, Any>)["name"] as! String
            
            self.location.text = city + ", " + region + ", " + country
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.profileArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfessionalProfileCell", for: indexPath) as! ProfessionalProfileCell
        
        cell.item.text = self.profileArray[indexPath.row].title
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(indexPath.row == 0)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfessionalAbout") as! ProfessionalAbout
            
            viewController.userArray = self.userArray
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 1)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfessionalMedicalInformation") as! ProfessionalMedicalInformation
            
            viewController.userArray = self.userArray
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 2)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfessionalReviews") as! ProfessionalReviews
            
            viewController.professionalReviewsDelegate = self
            
            viewController.userArray = self.userArray
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
    @IBAction func sendMessageButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "ComposeMessageController") as! ComposeMessageController
        
        viewController.username = self.username
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func refreshUser(completionHandler:@escaping (_ result: Dictionary<String, Any>) -> Void) {
        
        var returnedResults = Dictionary<String, Any>()
        
        getProfessional(username: self.username){ (result: Dictionary<String, Any>) in
         
            DispatchQueue.main.async {
            
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
        
    }
    
    func getProfessional(username: String, completionHandler:@escaping (_ result: Dictionary<String, Any>) -> Void)
    {
        var returnedResults = Dictionary<String, Any>()
        
        APIController().getUserByUsername(username: username)
        {
            (result: Dictionary<String, Any>) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
}
