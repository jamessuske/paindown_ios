//
//  TouchIDAuthentication.swift
//  Paindown
//
//  Created by James Suske on 2018-08-11.
//  Copyright © 2018 James Suske. All rights reserved.
//

import Foundation
import LocalAuthentication

class TouchIDAuth {
    
    let context = LAContext()
    
    func canEvaluatePolicy() -> Bool {
        return context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    }
    
    func authenticateUser(completion: @escaping (NSNumber?) -> Void) {
        
        guard canEvaluatePolicy() else {
            completion(0)
            return
        }
        
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Logging in with Touch ID") { (success, evaluateError) in
            if success {
                DispatchQueue.main.async {
                    completion(nil)
                }
            } else {
                
                let response: NSNumber
                
                switch evaluateError?._code {
                case Int(kLAErrorAuthenticationFailed):
                    response = 2
                case Int(kLAErrorUserCancel):
                    response = 3
                case Int(kLAErrorUserFallback):
                    response = 4
                default:
                    response = 1
                }
                
                completion(response)
                
            }
        }
    }
    
}

