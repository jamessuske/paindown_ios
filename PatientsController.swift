//
//  PatientController.swift
//  Paindown
//
//  Created by James Suske on 2018-07-21.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage

class PatientsController: UICollectionViewController, PatientsFilterControllerDelegate {
    
    var patients = Array<Dictionary<String, Any>>()
    
    @IBOutlet var menuButton: UIBarButtonItem!
    
    @IBOutlet var mapButton: UIBarButtonItem!
    
    @IBOutlet var filterButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        
        self.collectionView.dataSource = self
        
        getPatients(){ result in
            
            self.patients = result
            
            self.collectionView.reloadData()
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return patients.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "patientCell", for: indexPath) as! PatientCell

        cell.cellText?.text = (self.patients[indexPath.row]["username"] as! String)
        cell.cellDescription?.text = (self.patients[indexPath.row]["story"] as! String)
        cell.cellTitle?.text = (self.patients[indexPath.row]["pain_type"] as! String) + " | " + (self.patients[indexPath.row]["pain_level"] as! String) + " " + (self.patients[indexPath.row]["pain_years"] as! String) + " years with pain"
        
        cell.cellImage?.layer.borderWidth = 1
        cell.cellImage?.layer.masksToBounds = false
        cell.cellImage?.layer.borderColor = UIColor.clear.cgColor
        cell.cellImage?.layer.cornerRadius = (cell.cellImage?.frame.height)! / 2
        cell.cellImage?.clipsToBounds = true
        
        cell.contentView.layer.cornerRadius = 10.0
        
        if((self.patients[indexPath.row]["image"] is NSNull) == false) {
            
            let imgUrl = URL(string:(self.patients[indexPath.row]["image"] as! String))
            
            cell.cellImage?.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
            
        }
        else
        {
            cell.cellImage.image = UIImage(named: "default.png")
        }
        
        
        //cell.cellDescription?.numberOfLines = 0
        
        //cell.cellDescription?.lineBreakMode = .byWordWrapping
        
        return cell
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 150)
    }
    
    @IBAction func filterButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "PatientsFilterController") as! PatientsFilterController
        
        viewController.patientsFilterControllerDelegate = self
        
        let menuRightNavigationController = UISideMenuNavigationController(rootViewController: viewController)
        
        SideMenuManager.default.menuRightNavigationController = menuRightNavigationController
        
        present(menuRightNavigationController, animated: true, completion: nil)
        
    }
    
    @IBAction func mapButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "PatientsMapController") as! PatientsMapController
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func patientsFilterControllerDelegateMethod(order: String, location: String, disMin: String, disMax: String, painMin: String, painMax: String, pain: String, painkillers: String, lat: String, long: String) {
        
        getPatientsFilters(order: order, location: location, disMin: disMin, disMax: disMax, painMin: painMin, painMax: painMax, painKillers: painkillers, painType: pain, long: long, lat: lat){ result in
            
            self.patients = result
            
            self.collectionView.reloadData()
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "patientsSegue" {

            let cell = sender as! PatientCell
            
            let controller = segue.destination as! PatientProfileController
            
            if let indexPath = self.collectionView!.indexPath(for: cell) {
                
                controller.username = self.patients[indexPath.row]["username"] as! String
                
            }
            
        }
    }
    
    func getPatients(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getPatients()
        {
            (result: Array<Dictionary<String, Any>>) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
    func getPatientsFilters(order: String, location: String, disMin: String, disMax: String, painMin: String, painMax: String, painKillers: String, painType: String, long: String, lat: String, completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getPatientsFilters(painType: painType, location: location, order: order, disMin: disMin, disMax: disMax, painMin: painMin, painMax: painMax, painKillers: painKillers, long: long, lat: lat)
        {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
}
