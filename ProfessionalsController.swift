//
//  ProfessionalController.swift
//  Paindown
//
//  Created by James Suske on 2018-07-21.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage

class ProfessionalsController: UICollectionViewController, ProfessionalsFilterControllerDelegate {
    
    var professionals = Array<Dictionary<String, Any>>()
    
    @IBOutlet var menuButton: UIBarButtonItem!
    
    @IBOutlet var filterButton: UIBarButtonItem!
    
    @IBOutlet var mapButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        
        self.collectionView.dataSource = self
        
        getProfessionals(){ result in
            
            self.professionals = result
            
            self.collectionView.reloadData()
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return professionals.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "professionalCell", for: indexPath) as! ProfessionalCell
        
        if((self.professionals[indexPath.row]["image"] is NSNull) == false) {
            
            let imgUrl = URL(string:(self.professionals[indexPath.row]["image"] as! String))
            
            cell.cellImage?.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
            
        }
        else
        {
            cell.cellImage.image = UIImage(named: "default.png")
        }
        
        cell.cellText?.text = (self.professionals[indexPath.row]["username"] as! String)
        cell.cellDescription?.text = (self.professionals[indexPath.row]["about"] as! String)
        cell.cellTitle?.text = (self.professionals[indexPath.row]["profession_name"] as! String)
        
        cell.cellImage?.layer.borderWidth = 1
        cell.cellImage?.layer.masksToBounds = false
        cell.cellImage?.layer.borderColor = UIColor.clear.cgColor
        cell.cellImage?.layer.cornerRadius = (cell.cellImage?.frame.height)! / 2
        cell.cellImage?.clipsToBounds = true
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 150)
    }
    
    @IBAction func menuButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "MenuController") as! MenuController
        
        let menuRightNavigationController = UISideMenuNavigationController(rootViewController: viewController)
        
        SideMenuManager.default.menuRightNavigationController = menuRightNavigationController
        
        present(menuRightNavigationController, animated: true, completion: nil)
        
    }
    
    @IBAction func filterButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfessionalsFilterController") as! ProfessionalsFilterController
        
        viewController.professionalsFilterControllerDelegate = self
        
        let menuRightNavigationController = UISideMenuNavigationController(rootViewController: viewController)
        
        SideMenuManager.default.menuRightNavigationController = menuRightNavigationController
        
        present(menuRightNavigationController, animated: true, completion: nil)
        
    }
    
    @IBAction func mapButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfessionalMapController") as! ProfessionalMapController
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func professionalsFilterControllerDelegateMethod(order: String, location: String, disMin: String, disMax: String, pros: String, spec: String, lat: String, long: String) {
        
        print("HEEEEERRRREEE")
        
        getProfessionalsFilters(pros: pros, spec: spec, location: location, order: order, disMin: disMin, disMax: disMax, long: long, lat: lat){ result in
            
         
            self.professionals = result
            
            self.collectionView.reloadData()
            
            
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "professionalSegue" {

            let cell = sender as! ProfessionalCell
            
            let controller = segue.destination as! ProfessionalProfileController
            
            if let indexPath = self.collectionView!.indexPath(for: cell) {
                
                controller.username = self.professionals[indexPath.row]["username"] as! String
                
            }
            
        }
    }
    
    func getProfessionals(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getProfessionals()
        {
            (result: Array<Dictionary<String, Any>>) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
    func getProfessionalsFilters(pros: String, spec: String, location: String, order: String, disMin: String, disMax: String, long: String, lat: String, completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getProfessionalsFilters(pros: pros, spec: spec, location: location, order: order, disMin: disMin, disMax: disMax, long: long, lat: lat)
        {
            (result: Array<Dictionary<String, Any>>) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
}
