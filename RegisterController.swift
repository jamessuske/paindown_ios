//
//  RegisterController.swift
//  Paindown
//
//  Created by James Suske on 2018-07-23.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit

class RegisterController: UICollectionViewController {
    
    var registerItems = Array<String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        
        self.collectionView.dataSource = self
        
        registerItems = ["Professional", "Patients"]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return registerItems.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RegistrationCell", for: indexPath) as! RegistrationCell
        
        cell.registerLabel.text = self.registerItems[indexPath.row]
        
        return cell
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(self.registerItems[indexPath.row] == "Professional")
        {
            
            let viewController = storyboard?.instantiateViewController(withIdentifier: "professionalRegistration") as! ProfessionalRegistration
            
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
        else if(self.registerItems[indexPath.row] == "Patients")
        {
            
            let viewController = storyboard?.instantiateViewController(withIdentifier: "patientsRegistration") as! PatientsRegistration
            
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
        
    }
    
}
