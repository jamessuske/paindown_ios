//
//  PatientProfileController.swift
//  Paindown
//
//  Created by James Suske on 2018-08-31.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit
import SDWebImage

class PatientProfileController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var sendMessageButton: UIButton!
    
    @IBOutlet var user: UILabel!
    
    @IBOutlet var location: UILabel!
    
    @IBOutlet var profileCollection: UICollectionView!
    
    var profileArray = Array<PatientsProfileClass>()
    
    var username = String()
    
    var userArray = Dictionary<String, Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.profileArray.append(PatientsProfileClass(title: "Condition", controller: "PatientCondition"))
        self.profileArray.append(PatientsProfileClass(title: "Painkillers", controller: "PatientPainkillers"))
        self.profileArray.append(PatientsProfileClass(title: "Pain Story", controller: "PatientPainStory"))
        
        self.profileCollection.delegate = self
        self.profileCollection.dataSource = self
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = (profileImage.frame.height) / 2
        profileImage.clipsToBounds = true
        
        getPatient(username: self.username){ result in

            self.userArray = result
            
            if((result["image"] is NSNull) == false) {
                
                let imgUrl = URL(string:(result["image"] as! String))
                
                self.profileImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
                
            }
            else
            {
                self.profileImage.image = UIImage(named: "default.png")
            }
            
            self.user.text = (result["username"] as! String)
            
            let city = ((result["location"] as! Dictionary<String, Any>)["city"] as! Dictionary<String, Any>)["name"] as! String
            let country = ((result["location"] as! Dictionary<String, Any>)["country"] as! Dictionary<String, Any>)["name"] as! String
            let region = ((result["location"] as! Dictionary<String, Any>)["region"] as! Dictionary<String, Any>)["name"] as! String
            
            self.location.text = city + ", " + region + ", " + country
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.profileArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PatientsProfileCell", for: indexPath) as! PatientsProfileCell
        
        cell.item.text = self.profileArray[indexPath.row].title
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(indexPath.row == 0)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "PatientCondition") as! PatientCondition
            
            viewController.userArray = self.userArray
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 1)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "PatientPainkillers") as! PatientPainkillers
            
            viewController.userArray = self.userArray
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 2)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "PatientPainStory") as! PatientPainStory
            
            viewController.userArray = self.userArray
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
    @IBAction func sendMessageButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "ComposeMessageController") as! ComposeMessageController
        
        viewController.username = self.username
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func getPatient(username: String, completionHandler:@escaping (_ result: Dictionary<String, Any>) -> Void)
    {
        var returnedResults = Dictionary<String, Any>()
        
        APIController().getUserByUsername(username: username)
        {
                (result: Dictionary<String, Any>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
}
