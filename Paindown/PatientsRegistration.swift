//
//  PatientsRegistration.swift
//  Paindown
//
//  Created by James Suske on 2019-04-14.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class PatientsRegistration: UIPageViewController, UIPageViewControllerDataSource, PatientsUserDelegate, PatientsLocationDelegate, PatientsPainDelegate, PatientsPainkillersDelegate, PatientsPainStoryDelegate {
    
    var patient = PatientsRegisterClass()
    
    var indicator: UIActivityIndicatorView!
    
    lazy var orderedViewControllers: [UIViewController] = {
        return [
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PatientsUserController") as! PatientsUserController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PatientsLocationController") as! PatientsLocationController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PatientsPainController") as! PatientsPainController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PatientsPainkillersController") as! PatientsPainkillersController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PatientsPainStoryController") as! PatientsPainStoryController,
            
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        
        let patientsUserController = orderedViewControllers[0] as! PatientsUserController
        let patientsLocationController = orderedViewControllers[1] as! PatientsLocationController
        let patientsPainController = orderedViewControllers[2] as! PatientsPainController
        let patientsPainkillersController = orderedViewControllers[3] as! PatientsPainkillersController
        let patientsPainStoryController = orderedViewControllers[4] as! PatientsPainStoryController
        
        patientsUserController.patientsUserDelegate = self
        patientsLocationController.patientsLocationDelegate = self
        patientsPainController.patientsPainDelegate = self
        patientsPainkillersController.patientsPainkillersDelegate = self
        patientsPainStoryController.patientsPainStoryDelegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func patientsUserDelegateMethod(username: String, email: String, password: String, imageUrl: String) {
        
        patient.user_name = username
        patient.email = email
        patient.password = password
        patient.patient_image = imageUrl
        
        setViewControllers([orderedViewControllers[1]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
        
    }
    
    func patientsLocationDelegateMethod(fkcountry: Int, fkregion: Int, fkcity: Int, address: String, city_name: String) {
        
        patient.fkcountry = fkcountry
        patient.fkregion = fkregion
        patient.fkcity = fkcity
        patient.address = address
        patient.city_name = city_name
        
        setViewControllers([orderedViewControllers[2]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
        
    }
    
    func patientsPainDelegateMethod(fkpaintype: String, fkpainlevel: String, patient_painyears: Int) {
        
        patient.fkpaintype = fkpaintype
        patient.fkpainlevel = fkpainlevel
        patient.patient_painyears = patient_painyears
        
        setViewControllers([orderedViewControllers[3]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
        
    }
    
    func patientsPainkillersDelegateMethod(usage_painkillers: Bool, painkiller: Int, other_painkiller: String) {
        
        patient.usage_painkillers = usage_painkillers
        patient.painkiller = painkiller
        patient.other_painkiller = other_painkiller
        
        setViewControllers([orderedViewControllers[4]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
        
    }
    
    func patientsPainStoryDelegateMethod(is_public: Bool, patient_painstory: String) {
        
        patient.is_public = is_public
        patient.patient_painstory = patient_painstory
        
        self.createIndicator()
        
        self.patientRegister(patient: self.patient){ result in
            
            if(result == "")
            {
                self.customAlert(title: "Error", message: "There was an issue with the registration, please try again later.")
            }
            else
            {
                self.customAlert(title: "Success", message: result)
            }
            
            self.stopIndicator()
            
        }
        
    }
    
    func patientsLocationDelegateBackMethod() {
        
        setViewControllers([orderedViewControllers[0]],
                           direction: .reverse,
                           animated: true,
                           completion: nil)
        
    }
    
    func patientsPainDelegateBackMethod() {
        
        setViewControllers([orderedViewControllers[1]],
                           direction: .reverse,
                           animated: true,
                           completion: nil)
        
    }
    
    func patientsPainkillersBackDelegateMethod() {
        
        setViewControllers([orderedViewControllers[2]],
                           direction: .reverse,
                           animated: true,
                           completion: nil)
        
    }
    
    func patientsPainStoryBackDelegateMethod() {
        
        setViewControllers([orderedViewControllers[3]],
                           direction: .reverse,
                           animated: true,
                           completion: nil)
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
        
    }
    
    func patientRegister(patient: PatientsRegisterClass, completionHandler:@escaping (_ result:String) -> Void)
    {
        var returnedResults = String()
        
        APIController().patientResgister(patient)
        {
                (result: String) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    func createIndicator()
    {
        
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100)) as UIActivityIndicatorView
        
        indicator.center = self.view.center
        
        indicator.hidesWhenStopped = true
        
        indicator.style = UIActivityIndicatorView.Style.white
        
        indicator.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        
        indicator.layer.cornerRadius = 15
        
        self.view.addSubview(indicator)
        
        self.indicator.startAnimating()
    }
    
    func stopIndicator()
    {
        DispatchQueue.main.async {
            
            self.indicator.stopAnimating()
            self.indicator.removeFromSuperview()
            
        }
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
