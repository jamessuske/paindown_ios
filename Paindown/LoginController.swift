//
//  ViewController.swift
//  Paindown
//
//  Created by James Suske on 2018-07-19.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit

struct KeychainConfiguration {
    static let serviceName = "Paindown"
    static let accessGroup: String? = nil
}

class LoginController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var username: UITextField!
    
    @IBOutlet var password: UITextField!
    
    @IBOutlet var loginButton: UIButton!

    var passwordItems: [KeychainPasswordItem] = []

    let createLoginButtonTag = 0
    let loginButtonTag = 1
    let loginWithTouchID = 2

    let touchMe = TouchIDAuth()
    
    var indicator: UIActivityIndicatorView!
    
    @IBOutlet var forgotPassword: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let forgotTapGesture = UITapGestureRecognizer(target: self, action: #selector(LoginController.forgotTap))
        self.forgotPassword.isUserInteractionEnabled = true
        self.forgotPassword.addGestureRecognizer(forgotTapGesture)
        
        //Configure Username Text Box
        username.autocorrectionType = .no
        username.returnKeyType = .next
        username.delegate = self
        username.placeholder = "Email"
        
        //Configure Password Text Box
        password.autocorrectionType = .no
        password.isSecureTextEntry = true
        password.returnKeyType = .done
        password.delegate = self
        password.placeholder = "Password"
        
        //Define hasLogin variable, this variable returns a bool from the App Defaults to determine if a user has login or not before
        let hasLogin = UserDefaults.standard.bool(forKey: "hasLoginKey")
        
        //If hasLogin is true, change the button tag to 1 to indicate we do not need to create a new user, if false, change the button tag to 0 to indicate we need to create a new user.
        if hasLogin {
            loginButton.tag = loginButtonTag
        } else {
            loginButton.tag = createLoginButtonTag
        }
        
        //Check if user exists in Keychain
        if let storedUsername = UserDefaults.standard.value(forKey: "username") as? String {
            
            //Update the username text field with the username in keychain
            username.text = storedUsername
            
            //Check if device is compatible with Touch ID
            if(touchMe.canEvaluatePolicy())
            {
                
                //Change the Login button label to Login with Touch ID
                loginButton.setTitle("Login with Touch ID", for: .normal)
                
                //Update the Login button tag, this is if the user presses cancel on the Touch ID popup and then decides to login with Touch ID after.
                loginButton.tag = loginWithTouchID
                
                //Get Response from Touch ID popup
                touchMe.authenticateUser() { responsCode in
                    
                    if let responsCode = responsCode {
                        
                        if(responsCode == 0)
                        {
                            //If Touch ID is not available
                            self.customAlert(title: "Error", message: "Touch ID not available")
                        }
                        else if(responsCode == 1)
                        {
                            //If Touch ID has not been setup
                            self.customAlert(title: "Error", message: "Touch ID may not be configured")
                        }
                        else if(responsCode == 2)
                        {
                            //If Touch ID authentication failed
                            self.customAlert(title: "Error", message: "There was a problem verifying your identity")
                        }
                        
                    } else {
                        
                        //If there is no response code, that means Touch ID was successful in authenticating user and we can now call the login method
                        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(LoginController.login), userInfo: nil, repeats: false)
                    }
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        
        //Define Button variable from the button that has been tapped.
        let button = sender as! UIButton
        
        //If the button tag is Touch ID, authenticate the user
        
        if(button.tag == loginWithTouchID)
        {
            //Check if device is compatible with Touch ID
            if(touchMe.canEvaluatePolicy())
            {
                //Get Response from Touch ID popup
                touchMe.authenticateUser() { responsCode in
                    
                    if let responsCode = responsCode {
                        
                        if(responsCode == 0)
                        {
                            //If Touch ID is not available
                            self.customAlert(title: "Error", message: "Touch ID not available")
                        }
                        else if(responsCode == 1)
                        {
                            //If Touch ID has not been setup
                            self.customAlert(title: "Error", message: "Touch ID may not be configured")
                        }
                        else if(responsCode == 2)
                        {
                            //If Touch ID authentication failed
                            self.customAlert(title: "Error", message: "There was a problem verifying your identity")
                        }
                        
                    } else {
                        
                        //If there is no response code, that means Touch ID was successful in authenticating user and we can now call the login method
                        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(LoginController.login), userInfo: nil, repeats: false)
                    }
                }
            }
        }
        else
        {
            Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(LoginController.login), userInfo: nil, repeats: false)
        }
        
    }
    
    @objc func login() {
        
        //Start Activity Indicator
        
        self.createIndicator()
        
        //Define Username and Password Variables
        
        var user: String!
        var pass: String!
        
        
        //Check if User is Authenticating with TouchID, we do this so we know to use credentials from Keychain to make the API call with
        if(loginButton.tag == loginWithTouchID)
        {
            //If Yes, Get the username from Keychain
            if let storedUsername = UserDefaults.standard.value(forKey: "username") as? String {
                
                //Get Password from Keychain
                
                do {
                    
                    let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                            account: storedUsername,
                                                            accessGroup: KeychainConfiguration.accessGroup)
                    let keychainPassword = try passwordItem.readPassword()
                    
                    //Store Username and Password from Keychain into Username and Password variables
                    
                    user = storedUsername
                    pass = keychainPassword
                    
                }
                catch {
                    
                    //If something went wrong, stop the Activity Indicator and Alert the user something went wrong.
                    
                    self.stopIndicator()
                    
                    self.customAlert(title: "Error", message: "Error reading password from keychain - \(error)")
                }
                
            }
        }
        else
        {
            //If we are not using Touch ID, store the username and password text field into the username and password variable to use for the API Call
            
            user = username.text!
            pass = password.text!
        }
        
        //Finally call the webservice
        
        APIController().loginUser(email: user, password: pass)
        {
            (result: String) in
            //If API call is successful
            if(result == "")
            {
                
                //Stop Activity Indicator
                
                self.stopIndicator()
                
                //Check if button tag is create, login or touch ID
                
                if self.loginButton.tag == self.createLoginButtonTag {
                    
                    //If create, check if a user has login
                    
                    let hasLoginKey = UserDefaults.standard.bool(forKey: "hasLoginKey")
                    if !hasLoginKey {
                        
                        //If not, add username to App Default
                        
                        UserDefaults.standard.setValue(user, forKey: "username")
                    }
                    
                    //Try and save the password to Keychain
                    
                    do {
                        
                        //Create a KeychainPasswordItem
                        
                        let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName, account: user!, accessGroup: KeychainConfiguration.accessGroup)
                        
                        //Save password to the new KeychainPasswordItem
                        
                        try passwordItem.savePassword(pass!)
                        
                        //Add hasLoginKey bool to App Defaults
                        
                        UserDefaults.standard.set(true, forKey: "hasLoginKey")
                        
                        //Change Login button tag to Login as we do not need to create this user again
                        
                        self.loginButton.tag = self.loginButtonTag
                        
                        //Everything has been authenticated, proceed to Dashboad
                        
                        self.stopIndicator()
                        
                        self.navigationController?.popViewController(animated: true)
                        
                        
                    } catch {
                        
                        //Something went wrong, alert the user with error.
                        
                        self.customAlert(title: "Error", message: "Error updating keychain - \(error)")
                        
                    }
                    
                }
                    //If Login Button tag with Login
                else if self.loginButton.tag == self.loginButtonTag {
                    
                    //Check if user exists in Keychain
                    
                    if self.checkLogin(username: user, password: pass) {
                        
                        //Exisiting user has been authenticated, proceed to Dashboad
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    } else {
                        
                        //User does not exist in Keychain, alert user there is an error.
                        
                        self.customAlert(title: "Login Problem", message: "Sorry Login Failed, User and/or Passsword Incorrect")
                    }
                    
                }
                    //If Login Button tag with Touch ID
                else if self.loginButton.tag == self.loginWithTouchID {
                    
                    //Touch ID has been authenticated, proceed to Dashboad
                    
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
            }
            else
            {
                
                //Stop Activity Indicator
                
                self.stopIndicator()
                
                //API call was unsuccessful, alert user.
                
                self.customAlert(title: "Login Problem", message: result)
                
            }
            
        }
        
    }
    
    func checkLogin(username: String, password: String) -> Bool {
        
        guard username == UserDefaults.standard.value(forKey: "username") as? String else {
            return false
        }
        
        do {
            
            let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                    account: username,
                                                    accessGroup: KeychainConfiguration.accessGroup)
            
            let keychainPassword = try passwordItem.readPassword()
            
            if(password == keychainPassword)
            {
                return true
            }
            else
            {
                
                if let storedUsername = UserDefaults.standard.value(forKey: "username") as? String {
                    
                    let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                            account: storedUsername,
                                                            accessGroup: KeychainConfiguration.accessGroup)
                    try passwordItem.savePassword(password)
                    
                }
                
                return true
            }
        }
        catch {
            self.customAlert(title: "Error", message: "Error reading password from keychain - \(error)")
        }
        
        return false
    }
    
    @objc func forgotTap(sender:UITapGestureRecognizer) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "Forgot") as! ForgotController
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func textFieldShouldBeginEditing(_ state: UITextField) -> Bool {
        
        //Check keychain if a user has already login in
        
        let hasLogin = UserDefaults.standard.bool(forKey: "hasLoginKey")
        
        //If yes, change loginButton tag to login, else change loginButton to create
        
        if hasLogin {
            loginButton.tag = loginButtonTag
        } else {
            loginButton.tag = createLoginButtonTag
        }
        
        //Change login Button text to Login
        
        loginButton.setTitle("Login", for: .normal)
        
        return true
        
    }
    
    /**
     Keyboard Next or Done Button Pressed method.
     */
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //When User hits the return or done button on the keyboard.
        
        if (textField == self.username)
        {
            //If the text field is username, make the password text field the first responder
            
            password.becomeFirstResponder()
            
            //Check if username is in Keychain
            
            if let storedUsername = UserDefaults.standard.value(forKey: "username") as? String {
                
                //If yes, change loginButton tag to login, else change loginButton to create
                
                if(storedUsername == self.username.text)
                {
                    loginButton.tag = loginButtonTag
                }
                else
                {
                    loginButton.tag = createLoginButtonTag
                }
                
            }
        }
        
        if(textField == self.password)
        {
            //Check if username is in Keychain
            
            if let storedUsername = UserDefaults.standard.value(forKey: "username") as? String {
                
                //If yes, change loginButton tag to login, else change loginButton to create
                
                if(storedUsername == self.username.text)
                {
                    loginButton.tag = loginButtonTag
                }
                else
                {
                    loginButton.tag = createLoginButtonTag
                }
                
            }
            
            //If the text field is password, then we are done editing, dismiss the keyboard.
            
            self.view.endEditing(true)
            
            //Call login method
            
            Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(LoginController.login), userInfo: nil, repeats: false)
            
        }
        
        return true
    }
    
    func createIndicator()
    {
        
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100)) as UIActivityIndicatorView
        
        indicator.center = self.view.center
        
        indicator.hidesWhenStopped = true
        
        indicator.style = UIActivityIndicatorView.Style.white
        
        indicator.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        
        indicator.layer.cornerRadius = 15
        
        self.view.addSubview(indicator)
        
        self.indicator.startAnimating()
    }
    
    func stopIndicator()
    {
        DispatchQueue.main.async {
            
            self.indicator.stopAnimating()
            self.indicator.removeFromSuperview()
            
        }
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
