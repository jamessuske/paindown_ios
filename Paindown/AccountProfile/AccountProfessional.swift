//
//  AccountProfessional.swift
//  Paindown
//
//  Created by James Suske on 2019-06-18.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class AccountProfessional: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, AccountProfessionalProfessionDelegate {
    
    var userProfile = Dictionary<String, Any>()
    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var username: UILabel!
    
    @IBOutlet var collectionView: UICollectionView!
    
    var accountArray = Array<AccountClass>()
    
    var userData = UsersProfessionalClass()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        userData.about = userProfile["about"] as! String
        
        userData.approach = userProfile["approach"] as! String
        
        userData.email = userProfile["email"] as! String
        
        if(userProfile["image"] is NSNull)
        {
            userData.image = ""
        }
        else
        {
            userData.image = userProfile["image"] as! String
        }
        
        userData.isPainkillers = false

        userData.location = userProfile["location"] as! Dictionary<String, Any>
        
        userData.membership = userProfile["membership"] as! Int
        
        userData.membership_date = ""
        
        userData.name = userProfile["name"] as! String
        
        userData.phone = userProfile["phone"] as! String
        
        userData.preference = userProfile["preference"] as! Array<Dictionary<String, Any>>
        
        userData.profession = userProfile["profession"] as! Int
        
        userData.isPublic = false
        
        userData.stories = userProfile["stories"] as! String
        
        userData.specialities = userProfile["specialities"] as! Array<Dictionary<String, Any>>
            
        userData.qualifications = userProfile["qualifications"] as! Array<Dictionary<String, Any>>
        
        userData.user_name = userProfile["username"] as! String
        
        userData.website = userProfile["website"] as! String
        
        self.collectionView.delegate = self
        
        self.collectionView.dataSource = self
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = (profileImage.frame.height) / 2
        profileImage.clipsToBounds = true
        
        if(userProfile["image"] is NSNull)
        {
            self.profileImage.image = UIImage(named: "default.png")
        }
        else
        {
            let imgUrl = URL(string:(userProfile["image"] as! String))
            self.profileImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
        }
        
        self.username.text = (userProfile["username"] as! String)
        
        accountArray.append(AccountClass(title: "Location", controller: "AccountProfessionalLocation"))
        accountArray.append(AccountClass(title: "Pain", controller: "AccountProfessionalProfession"))
        accountArray.append(AccountClass(title: "About Me", controller: "AccountProfessionalAbout"))
        accountArray.append(AccountClass(title: "My Approach", controller: "AccountProfessionalApproach"))
        accountArray.append(AccountClass(title: "My Story", controller: "AccountProfessionalStory"))
        accountArray.append(AccountClass(title: "Specialities", controller: "AccountProfessionalsSpecialities"))
        accountArray.append(AccountClass(title: "Qualifications", controller: "AccountProfessionalsQualifications"))
        
        self.collectionView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.accountArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "professionalAccountCell", for: indexPath) as! AccountCell
        
        cell.item.text = self.accountArray[indexPath.row].title
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(indexPath.row == 0)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountProfessionalLocation") as! AccountProfessionalLocation
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 1)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountProfessionalProfession") as! AccountProfessionalProfession
            
            viewController.accountProfessionalProfessionDelegate = self
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 2)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountProfessionalAbout") as! AccountProfessionalAbout
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 3)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountProfessionalApproach") as! AccountProfessionalApproach
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 4)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountProfessionalStory") as! AccountProfessionalStory
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 5)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountProfessionalsSpecialities") as! AccountProfessionalsSpecialities
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 6)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountProfessionalsQualifications") as! AccountProfessionalsQualifications
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
    func accountProfessionalProfessionDelegateMethod(fkprofession: String, professional_website: String) {
        
        userData.profession = Int(fkprofession)!
        userData.website = professional_website

        updateProfessionals(professional: userData){ result in
            
            print(result)
            
        }
        
    }
    
    func updateProfessionals(professional: UsersProfessionalClass, completionHandler:@escaping (_ result:String) -> Void)
    {
        var returnedResults = String()
        
        APIController().updateProfessionals(cellHolder: professional)
        {
            (result: String) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
}
