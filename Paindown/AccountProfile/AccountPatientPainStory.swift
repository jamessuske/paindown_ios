//
//  AccountPatientPainStory.swift
//  Paindown
//
//  Created by James Suske on 2019-08-04.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class AccountPatientPainStory: UIViewController {
    
    var userProfile = Dictionary<String, Any>()
    
    @IBOutlet var publicStory: UISwitch!
    
    @IBOutlet var story: UITextView!
    
    @IBOutlet var saveButton: UIButton!
    
    var is_public = Bool()
    
    var patient_painstory = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let borderColor = UIColor(displayP3Red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
        
        story.layer.borderColor = borderColor.cgColor;
        story.layer.borderWidth = 1.0
        story.layer.cornerRadius = 5.0
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
    }
    
    @IBAction func publicStorySwitch(_ sender: Any) {
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
