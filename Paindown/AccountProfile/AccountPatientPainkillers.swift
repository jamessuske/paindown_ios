//
//  AccountPatientPainkillers.swift
//  Paindown
//
//  Created by James Suske on 2019-08-04.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class AccountPatientPainkillers: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    var userProfile = Dictionary<String, Any>()
    
    @IBOutlet var usesPainkillers: UISwitch!
    
    @IBOutlet var painkillers: UITextField!
    
    @IBOutlet var otherPainkillers: UITextField!
    
    @IBOutlet var saveButton: UIButton!
    
    let pickerView = UIPickerView()
    
    var pickOption = Array<Dictionary<String, Any>>()
    
    var usage_painkillers = Bool()
    
    var painkiller = Int()
    
    var other_painkiller = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        painkillers.delegate = self
        
        let painKillersGesture = UITapGestureRecognizer(target: self, action: #selector(painKillersTapped))
        
        painkillers.addGestureRecognizer(painKillersGesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getPainKillers(){ result in
            
            self.pickOption = result
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]["name"] as? String
    }
    
    @objc func painKillersTapped() {
        
        pickerView.delegate = self
        
        pickerView.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.donePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        painkillers.inputAccessoryView = toolBar
        
        view.addSubview(painkillers)
        
        painkillers.inputView = pickerView
        
        painkillers.becomeFirstResponder()
        
        
    }
    
    @objc func donePicker() {
        
        painkillers.resignFirstResponder()
        
        self.painkillers.text = pickOption[pickerView.selectedRow(inComponent: 0)]["name"] as? String
        
        painkiller = (pickOption[pickerView.selectedRow(inComponent: 0)]["id"] as? Int)!
        
    }
    
    @objc func cancelPicker() {
        
        painkillers.resignFirstResponder()
        
    }
    
    @IBAction func usesPainkillersSwitch(_ sender: Any) {
    }
    
    
    func getPainKillers(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getPainkillers()
            {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
