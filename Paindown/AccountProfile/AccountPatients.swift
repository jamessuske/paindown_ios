//
//  AccountPersonal.swift
//  Paindown
//
//  Created by James Suske on 2019-06-17.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class AccountPatients: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var userProfile = Dictionary<String, Any>()
    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var username: UILabel!
    
    @IBOutlet var collectionView: UICollectionView!
    
    var accountArray = Array<AccountClass>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(userProfile)
        
        self.collectionView.delegate = self
        
        self.collectionView.dataSource = self
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = (profileImage.frame.height) / 2
        profileImage.clipsToBounds = true
        
        let imgUrl = URL(string:(userProfile["image"] as! String))
        
        self.profileImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
        
        self.username.text = (userProfile["username"] as! String)
        
        accountArray.append(AccountClass(title: "Location", controller: "AccountPatientslLocation"))
        accountArray.append(AccountClass(title: "Pain", controller: "AccountPatientPain"))
        accountArray.append(AccountClass(title: "Painkillers", controller: "AccountPatientPainkillers"))
        accountArray.append(AccountClass(title: "Pain Story", controller: "AccountPatientPainStory"))
        
        self.collectionView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.accountArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "patientAccountCell", for: indexPath) as! AccountCell
        
        cell.item.text = self.accountArray[indexPath.row].title
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(indexPath.row == 0)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountPatientsLocation") as! AccountPatientsLocation
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 1)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountPatientPain") as! AccountPatientPain
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 2)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountPatientPainkillers") as! AccountPatientPainkillers
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 3)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountPatientPainStory") as! AccountPatientPainStory
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
}
