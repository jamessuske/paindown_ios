//
//  AccountAdvanced.swift
//  Paindown
//
//  Created by James Suske on 2019-06-17.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class AccountAdvanced: UIViewController {
    
    var userProfile = Dictionary<String, Any>()
    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var username: UILabel!
    
    @IBOutlet var saveButton: UIButton!
    
    @IBOutlet var smsSwitch: UISwitch!
    
    @IBOutlet var emailSwitch: UISwitch!
    
    var indicator: UIActivityIndicatorView!
    
    var userPreferences = Array<Dictionary<String, Any>>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userPreferences = userProfile["preference"] as! Array<Dictionary<String, Any>>

        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = (profileImage.frame.height) / 2
        profileImage.clipsToBounds = true
        
        let imgUrl = URL(string:(userProfile["image"] as! String))
        
        self.profileImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
        
        self.username.text = (userProfile["username"] as! String)
        
        for var perference in userPreferences {
            
            if(perference["pkpreference"] as! Int == 1)
            {
                smsSwitch.setOn((perference["patientpreference_selected"] as! Int) == 1 ? true : false, animated: false)
            }
            
            if(perference["pkpreference"] as! Int == 2)
            {
                emailSwitch.setOn((perference["patientpreference_selected"] as! Int) == 1 ? true : false, animated: false)
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        self.createIndicator()
        
        updatePerference(user: self.userPreferences){ result in
            
            if(result["message"] != nil)
            {
                self.customAlert(title: "Success", message: result["message"] as! String)
            }
            else
            {
                self.customAlert(title: "Error", message: "Something went wrong, please try again.")
            }
            
            self.stopIndicator()
            
        }
        
    }
    
    @IBAction func smsSwitchChanged(_ sender: UISwitch) {

        var loopCounter = 0
        
        for var perference in userPreferences {
            
            if(perference["pkpreference"] as! Int == 1)
            {
                userPreferences[loopCounter]["patientpreference_selected"] = sender.isOn == true ? 1 : 0
                
                loopCounter = loopCounter + 1
            }
            
        }
        
    }
    
    @IBAction func emailSwitchChanged(_ sender: UISwitch) {

        var loopCounter = 0
        
        for var perference in userPreferences {
            
            if(perference["pkpreference"] as! Int == 2)
            {
                userPreferences[loopCounter]["patientpreference_selected"] = sender.isOn == true ? 1 : 0
                
                loopCounter = loopCounter + 1
            }
            
        }
        
    }
    
    func updatePerference(user: Array<Dictionary<String, Any>>, completionHandler:@escaping (_ result:Dictionary<String, Any>) -> Void)
    {
        var returnedResults = Dictionary<String, Any>()
        
        APIController().updatePerference(user: user)
        {
            (result: Dictionary<String, Any>) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
    func createIndicator()
    {
        
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100)) as UIActivityIndicatorView
        
        indicator.center = self.view.center
        
        indicator.hidesWhenStopped = true
        
        indicator.style = UIActivityIndicatorView.Style.white
        
        indicator.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        
        indicator.layer.cornerRadius = 15
        
        self.view.addSubview(indicator)
        
        self.indicator.startAnimating()
    }
    
    func stopIndicator()
    {
        DispatchQueue.main.async {
            
            self.indicator.stopAnimating()
            self.indicator.removeFromSuperview()
            
        }
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}
