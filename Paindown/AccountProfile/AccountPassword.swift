//
//  AccountPassword.swift
//  Paindown
//
//  Created by James Suske on 2019-06-17.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class AccountPassword: UIViewController {
    
    var userProfile = Dictionary<String, Any>()
    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var username: UILabel!
    
    @IBOutlet var saveButton: UIButton!
    
    @IBOutlet var oldPassword: UITextField!
    
    @IBOutlet var newPassword: UITextField!
    
    @IBOutlet var confirmPassword: UITextField!
    
    var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = (profileImage.frame.height) / 2
        profileImage.clipsToBounds = true
        
        let imgUrl = URL(string:(userProfile["image"] as! String))
        
        self.profileImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
        
        self.username.text = (userProfile["username"] as! String)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        if(self.oldPassword.text == "")
        {
            self.customAlert(title: "Error", message: "Current Password can't be blank.")
            return
        }
        
        if(self.newPassword.text == "")
        {
            self.customAlert(title: "Error", message: "New Password can't be blank.")
            return
        }
        
        if(self.confirmPassword.text == "")
        {
            self.customAlert(title: "Error", message: "Password Confirm can't be blank.")
            return
        }
        
        if(self.newPassword.text != self.confirmPassword.text)
        {
            self.customAlert(title: "Error", message: "New Password does not match.")
            return
        }
        
        let usersPasswordClass = UsersPasswordClass(email: userProfile["email"] as! String, password: oldPassword.text!, newpassword: newPassword.text!, password_confirmation: confirmPassword.text!)
        
        self.createIndicator()
        
        updatePassword(user: usersPasswordClass){ result in
            
            print(result)
            
            if(result["message"] != nil)
            {
                self.customAlert(title: "Success", message: result["message"] as! String)
            }
            else
            {
                self.customAlert(title: "Error", message: "Something went wrong, please try again.")
            }
            
            self.stopIndicator()
            
        }
        
        
    }
    
    func updatePassword(user: UsersPasswordClass, completionHandler:@escaping (_ result:Dictionary<String, Any>) -> Void)
    {
        var returnedResults = Dictionary<String, Any>()
        
        APIController().updatePassword(user: user)
        {
                (result: Dictionary<String, Any>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    func createIndicator()
    {
        
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100)) as UIActivityIndicatorView
        
        indicator.center = self.view.center
        
        indicator.hidesWhenStopped = true
        
        indicator.style = UIActivityIndicatorView.Style.white
        
        indicator.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        
        indicator.layer.cornerRadius = 15
        
        self.view.addSubview(indicator)
        
        self.indicator.startAnimating()
    }
    
    func stopIndicator()
    {
        DispatchQueue.main.async {
            
            self.indicator.stopAnimating()
            self.indicator.removeFromSuperview()
            
        }
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
