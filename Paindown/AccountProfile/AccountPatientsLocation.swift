//
//  AccountPatientslLocation.swift
//  Paindown
//
//  Created by James Suske on 2019-08-04.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class AccountPatientsLocation: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var userProfile = Dictionary<String, Any>()
    
    @IBOutlet var countryText: UITextField!
    
    @IBOutlet var regionText: UITextField!
    
    @IBOutlet var cityText: UITextField!
    
    @IBOutlet var addressText: UITextField!
    
    @IBOutlet var saveButton: UIButton!
    
    let pickerViewCountry = UIPickerView()
    
    let pickerViewRegion = UIPickerView()
    
    let pickerViewCity = UIPickerView()
    
    var pickOption = Array<Dictionary<String, Any>>()
    
    var pickOptionRegion = Array<Dictionary<String, Any>>()
    
    var pickOptionCity = Array<Dictionary<String, Any>>()
    
    var fkcountry = Int()
    
    var fkregion = Int()
    
    var fkcity = Int()
    
    var city_name = String()
    
    var address = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let countryGesture = UITapGestureRecognizer(target: self, action: #selector(countryTapped))
        
        let regionGesture = UITapGestureRecognizer(target: self, action: #selector(regionTapped))
        
        let cityGesture = UITapGestureRecognizer(target: self, action: #selector(cityTapped))
        
        countryText.addGestureRecognizer(countryGesture)
        regionText.addGestureRecognizer(regionGesture)
        cityText.addGestureRecognizer(cityGesture)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.getCountries(){ result in
            
            self.pickOption = result
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        var count = 0
        
        if pickerView == pickerViewCountry
        {
            count = pickOption.count
        }
        else if pickerView == pickerViewRegion
        {
            count = pickOptionRegion.count
        }
        else if pickerView == pickerViewCity
        {
            count = pickOptionCity.count
        }
        
        return count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        var value = ""
        
        if pickerView == pickerViewCountry
        {
            value = (pickOption[row]["text"] as? String)!
        }
        else if pickerView == pickerViewRegion
        {
            value = (pickOptionRegion[row]["text"] as? String)!
        }
        else if pickerView == pickerViewCity
        {
            value = (pickOptionCity[row]["text"] as? String)!
        }
        
        return value
    }
    
    @objc func countryTapped() {
        
        pickerViewCountry.delegate = self
        
        pickerViewCountry.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.donePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        countryText.inputAccessoryView = toolBar
        
        view.addSubview(countryText)
        
        countryText.inputView = pickerViewCountry
        
        countryText.becomeFirstResponder()
        
        
    }
    
    @objc func regionTapped() {
        
        pickerViewRegion.delegate = self
        
        pickerViewRegion.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.regionDonePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.regionCancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        regionText.inputAccessoryView = toolBar
        
        view.addSubview(regionText)
        
        regionText.inputView = pickerViewRegion
        
        regionText.becomeFirstResponder()
        
        
    }
    
    @objc func cityTapped() {
        
        pickerViewCity.delegate = self
        
        pickerViewCity.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cityDonePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cityCancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        cityText.inputAccessoryView = toolBar
        
        view.addSubview(cityText)
        
        cityText.inputView = pickerViewCity
        
        cityText.becomeFirstResponder()
        
        
    }
    
    @objc func donePicker() {
        
        countryText.resignFirstResponder()
        
        self.countryText.text = pickOption[pickerViewCountry.selectedRow(inComponent: 0)]["text"] as? String
        
        fkcountry = (pickOption[pickerViewCountry.selectedRow(inComponent: 0)]["id"] as? Int)!
        
        getRegions(id: String(fkcountry)){ result in
            
            self.pickOptionRegion = result
            
        }
        
    }
    
    @objc func cancelPicker() {
        
        countryText.resignFirstResponder()
        
    }
    
    @objc func regionDonePicker() {
        
        regionText.resignFirstResponder()
        
        self.regionText.text = pickOptionRegion[pickerViewRegion.selectedRow(inComponent: 0)]["text"] as? String
        
        fkregion = (pickOptionRegion[pickerViewRegion.selectedRow(inComponent: 0)]["id"] as? Int)!
        
        getCities(id: String(fkregion)){ result in
            
            self.pickOptionCity = result
            
        }
        
    }
    
    @objc func regionCancelPicker() {
        
        regionText.resignFirstResponder()
        
    }
    
    @objc func cityDonePicker() {
        
        cityText.resignFirstResponder()
        
        self.cityText.text = pickOptionCity[pickerViewCity.selectedRow(inComponent: 0)]["text"] as? String
        
        fkcity = (pickOptionCity[pickerViewCity.selectedRow(inComponent: 0)]["id"] as? Int)!
        
        city_name = (pickOptionCity[pickerViewCity.selectedRow(inComponent: 0)]["text"] as? String)!
        
    }
    
    @objc func cityCancelPicker() {
        
        cityText.resignFirstResponder()
        
    }
    
    func getCountries(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getCountries()
            {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    func getRegions(id: String, completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getRegions(id: id)
        {
            (result: Array<Dictionary<String, Any>>) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
    func getCities(id: String, completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getCities(id: id)
        {
            (result: Array<Dictionary<String, Any>>) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
