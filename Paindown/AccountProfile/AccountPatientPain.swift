//
//  AccountPatientPain.swift
//  Paindown
//
//  Created by James Suske on 2019-08-04.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class AccountPatientPain: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var userProfile = Dictionary<String, Any>()
    
    @IBOutlet var painType: UITextField!
    
    @IBOutlet var painLevel: UITextField!
    
    @IBOutlet var painYearsLabel: UILabel!
    
    @IBOutlet var painYears: UISlider!
    
    @IBOutlet var saveButton: UIButton!
    
    let painTypePickerView = UIPickerView()
    
    var painTypePickOption = Array<Dictionary<String, Any>>()
    
    let painLevelPickerView = UIPickerView()
    
    var painLevelPickOption = Array<Dictionary<String, Any>>()
    
    var fkpaintype = String()
    
    var fkpainlevel = String()
    
    var patient_painyears = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let painTypeGesture = UITapGestureRecognizer(target: self, action: #selector(painTypeTapped))
        
        let painLevelGesture = UITapGestureRecognizer(target: self, action: #selector(painLevelTapped))
        
        painType.addGestureRecognizer(painTypeGesture)
        painLevel.addGestureRecognizer(painLevelGesture)
        
        painYearsLabel.text = "Number of years with pain: " + String(painYears.value)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getPainTypes(){ result in
            
            print(result)
            
            self.painTypePickOption = result
            
        }
        
        getPainLevels(){ result in
            
            self.painLevelPickOption = result
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(painType.isFirstResponder)
        {
            return painTypePickOption.count
        }
        else
        {
            return painLevelPickOption.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(painType.isFirstResponder)
        {
            return painTypePickOption[row]["name"] as? String
        }
        else
        {
            return painLevelPickOption[row]["name"] as? String
        }
    }
    
    @objc func painTypeTapped() {
        
        painTypePickerView.delegate = self
        
        painTypePickerView.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.painTypeDonePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.painTypeCancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        painType.inputAccessoryView = toolBar
        
        view.addSubview(painType)
        
        painType.inputView = painTypePickerView
        
        painType.becomeFirstResponder()
        
        
    }
    
    @objc func painLevelTapped() {
        
        painLevelPickerView.delegate = self
        
        painLevelPickerView.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.painLevelDonePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.painLevelCancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        painLevel.inputAccessoryView = toolBar
        
        view.addSubview(painLevel)
        
        painLevel.inputView = painLevelPickerView
        
        painLevel.becomeFirstResponder()
        
        
    }
    
    @objc func painTypeDonePicker() {
        
        painType.resignFirstResponder()
        
        self.painType.text = painTypePickOption[painTypePickerView.selectedRow(inComponent: 0)]["name"] as? String
        
        fkpaintype = String((painTypePickOption[painTypePickerView.selectedRow(inComponent: 0)]["id"] as? Int)!)
        
    }
    
    @objc func painTypeCancelPicker() {
        
        painType.resignFirstResponder()
        
    }
    
    @objc func painLevelDonePicker() {
        
        painLevel.resignFirstResponder()
        
        self.painLevel.text = painLevelPickOption[painLevelPickerView.selectedRow(inComponent: 0)]["name"] as? String
        
        fkpainlevel = String((painLevelPickOption[painLevelPickerView.selectedRow(inComponent: 0)]["id"] as? Int)!)
        
    }
    
    @objc func painLevelCancelPicker() {
        
        painLevel.resignFirstResponder()
        
    }
    
    @IBAction func painYearsChanged(_ sender: Any) {
        
        painYearsLabel.text = "Number of years with pain: " + String(painYears.value)
        
    }
    
    func getPainTypes(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getPainTypes()
            {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    func getPainLevels(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getPainLevels()
            {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
