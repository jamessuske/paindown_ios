//
//  ProfessionalMedicalInformation.swift
//  Paindown
//
//  Created by James Suske on 2019-06-17.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class ProfessionalMedicalInformation: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var userArray = Dictionary<String, Any>()
    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var username: UILabel!
    
    @IBOutlet var location: UILabel!

    @IBOutlet var painKillersTable: UITableView!
    
    var painkillersArray = Array<Dictionary<String, Any>>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.painKillersTable.separatorStyle = .none
        
        painKillersTable.delegate = self
        painKillersTable.dataSource = self
        
        painkillersArray.append(["label" : "Qualifications", "items" : userArray["qualifications"]! as! Array<Dictionary<String, Any>>])
        painkillersArray.append(["label" : "Specialities", "items" : userArray["specialities"]! as! Array<Dictionary<String, Any>>])
        
        print(userArray["specialities"]! as! Array<Dictionary<String, Any>>)
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = (profileImage.frame.height) / 2
        profileImage.clipsToBounds = true
        
        let imgUrl = URL(string:(userArray["image"] as! String))
        
        self.profileImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
        
        self.username.text = (userArray["username"] as! String)
        
        let city = ((userArray["location"] as! Dictionary<String, Any>)["city"] as! Dictionary<String, Any>)["name"] as! String
        let country = ((userArray["location"] as! Dictionary<String, Any>)["country"] as! Dictionary<String, Any>)["name"] as! String
        let region = ((userArray["location"] as! Dictionary<String, Any>)["region"] as! Dictionary<String, Any>)["name"] as! String
        
        self.location.text = city + ", " + region + ", " + country
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return painkillersArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (painkillersArray[section]["items"] as! Array<Dictionary<String, Any>>).count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionHeader = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        let sectionText = UILabel()
        
        sectionText.frame = CGRect.init(x: 5, y: 5, width: sectionHeader.frame.width-10, height: sectionHeader.frame.height-10)
        
        sectionHeader.backgroundColor = .white
        
        sectionText.textColor = UIColor(displayP3Red: 6.0/255.0, green: 182.0/255.0, blue: 216.0/255.0, alpha: 1.0)
        
        sectionText.font = .systemFont(ofSize: 18, weight: .bold)
        
        sectionText.text = painkillersArray[section]["label"] as? String
        
        sectionHeader.addSubview(sectionText)
        
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return painkillersArray[section]["label"] as? String
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "professionalMedicalInformation", for: indexPath)
        
        cell.textLabel!.text = (painkillersArray[indexPath.section]["items"] as! Array<Dictionary<String, Any>>)[indexPath.row]["name"] as? String
        
        return cell
        
    }
    
}
