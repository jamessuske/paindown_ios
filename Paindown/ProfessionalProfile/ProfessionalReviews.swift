//
//  ProfessionalReviews.swift
//  Paindown
//
//  Created by James Suske on 2019-06-17.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit
import Cosmos

protocol ProfessionalReviewsDelegate: NSObjectProtocol {
    
    func refreshUser(completionHandler:@escaping (_ result: Dictionary<String, Any>) -> Void)
}

class ProfessionalReviews: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, ReviewFooterDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var userArray = Dictionary<String, Any>()
    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var username: UILabel!
    
    @IBOutlet var location: UILabel!
    
    var reviews = Array<Dictionary<String, Any>>()
    
    @IBOutlet var reviewsCollection: UICollectionView!
    
    var dateFormat = DateFormatter()
    
    var indicator: UIActivityIndicatorView!
    
    var professionalReviewsDelegate: ProfessionalReviewsDelegate!
    
    var selectedProblem: Int!
    
    var problems = Array<Dictionary<String, Any>>()
    
    let pickerView = UIPickerView()
    
    var problem = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        problems = [["id": 0, "value" : "Problem treated"], ["id" : 1, "value" : "One"], ["id" : 2, "value" : "Two"], ["id" : 3, "value" : "Three"], ["id" : 4, "value" : "Four"], ["id" : 5, "value" : "Five"]]
        
        self.selectedProblem = 1
        
        self.reviews = self.userArray["reviews"] as! Array<Dictionary<String, Any>>

        reviewsCollection.delegate = self
        reviewsCollection.dataSource = self
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = (profileImage.frame.height) / 2
        profileImage.clipsToBounds = true
        
        let imgUrl = URL(string:(userArray["image"] as! String))
        
        self.profileImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
        
        self.username.text = (userArray["username"] as! String)
        
        let city = ((userArray["location"] as! Dictionary<String, Any>)["city"] as! Dictionary<String, Any>)["name"] as! String
        let country = ((userArray["location"] as! Dictionary<String, Any>)["country"] as! Dictionary<String, Any>)["name"] as! String
        let region = ((userArray["location"] as! Dictionary<String, Any>)["region"] as! Dictionary<String, Any>)["name"] as! String
        
        self.location.text = city + ", " + region + ", " + country
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.reviews.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
        
        cell.review.text = self.reviews[indexPath.row]["review_description"] as? String

        print(self.reviews[indexPath.row]["review_value"]!)
        
        cell.rating.rating = self.reviews[indexPath.row]["review_value"] as! Double
        
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let publishedDate = dateFormat.date(from: self.reviews[indexPath.row]["updated_at"] as! String)
        
        dateFormat.dateFormat = "d MMM yyyy"
        
        let publishedDateString = dateFormat.string(from: publishedDate!)
        
        cell.published.numberOfLines = 0
        
        cell.published.lineBreakMode = .byWordWrapping
        
        cell.published.text = "Published " + publishedDateString + " " + "By: " + String(self.reviews[indexPath.row]["from_username"] as! String)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        
        if(kind == UICollectionView.elementKindSectionFooter)
        {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ReviewsFooter", for: indexPath) as! ReviewFooterCell

            let borderColor = UIColor(displayP3Red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
            
            footerView.text.layer.borderColor = borderColor.cgColor;
            footerView.text.layer.borderWidth = 1.0
            footerView.text.layer.cornerRadius = 5.0

            footerView.reviewFooterDelegate = self
            
            pickerView.delegate = self
            
            pickerView.dataSource = self
            
            let toolBar = UIToolbar()
            
            toolBar.barStyle = UIBarStyle.default
            
            toolBar.isTranslucent = true
            
            toolBar.sizeToFit()
            
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(ProfessionalReviews.doneButtonPressed))
            
            doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
            
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(ProfessionalReviews.cancelButtonPressed))
            
            cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
            
            toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
            
            toolBar.isUserInteractionEnabled = true
            
            footerView.problem.inputAccessoryView = toolBar
            
            footerView.problem.inputView = pickerView
            
            footerView.rating.rating = 0.0
            
            problem = footerView.problem
            
            return footerView
        }
        else if(kind == UICollectionView.elementKindSectionHeader)
        {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ReviewHeader", for: indexPath) as! ReviewHeaderCell
            
            headerView.totalReviews.text = "Total Reviews: " + String(self.reviews.count)
            
            headerView.rating.rating = (Double(self.userArray["rating"] as! String))!
            
            return headerView
        }
        
        fatalError()
        
    }
    
    @objc func doneButtonPressed() {
        
        problem.resignFirstResponder()
        
        problem.text = problems[pickerView.selectedRow(inComponent: 0)]["value"] as? String
        
        selectedProblem = pickerView.selectedRow(inComponent: 0)
        
    }
    
    @objc func cancelButtonPressed() {
        problem.resignFirstResponder()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return problems.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return problems[row]["value"] as? String
        
    }
    
    func submitReviewDelegate(text: String, rating: Float) {
        
        self.createIndicator()
        
        postReview(problem: self.selectedProblem, text: text, rating: rating, user: self.username.text!) { result in

            if(result == "")
            {
                self.customAlert(title: "Error", message: "Something went wrong, please try again later.")
                
                self.stopIndicator()
            }
            else
            {
                self.professionalReviewsDelegate.refreshUser() { userResult in
                    
                    self.userArray = userResult
                    
                    self.reviews = self.userArray["reviews"] as! Array<Dictionary<String, Any>>
                    
                    self.customAlert(title: "Success", message: result)
                    
                    self.stopIndicator()
                    
                    self.reviewsCollection.reloadData()
                    
                }
            }
            
        }
        
    }
    
    func postReview(problem: Int, text: String, rating: Float, user: String, completionHandler:@escaping (_ result: String) -> Void)
    {
        var returnedResults = String()
        
        APIController().postReview(problem: problem, text: text, rating: rating, user: user)
        {
            (result: String) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
    func createIndicator()
    {
        
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100)) as UIActivityIndicatorView
        
        indicator.center = self.view.center
        
        indicator.hidesWhenStopped = true
        
        indicator.style = UIActivityIndicatorView.Style.white
        
        indicator.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        
        indicator.layer.cornerRadius = 15
        
        self.view.addSubview(indicator)
        
        self.indicator.startAnimating()
    }
    
    func stopIndicator()
    {
        DispatchQueue.main.async {
            
            self.indicator.stopAnimating()
            self.indicator.removeFromSuperview()
            
        }
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
