//
//  ProfessionalAbout.swift
//  Paindown
//
//  Created by James Suske on 2019-06-17.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class ProfessionalAbout: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var userArray = Dictionary<String, Any>()
    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var username: UILabel!
    
    @IBOutlet var location: UILabel!
    
    @IBOutlet var aboutCollection: UICollectionView!
    
    var aboutArray = Array<Dictionary<String, Any>>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        aboutCollection.delegate = self
        aboutCollection.dataSource = self
        
        aboutArray.append(["label" : "About me", "text" : userArray["about"]!])
        aboutArray.append(["label" : "My approach", "text" : userArray["approach"]!])
        aboutArray.append(["label" : "My success stories", "text" : userArray["stories"]!])
        
        aboutCollection.reloadData()
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = (profileImage.frame.height) / 2
        profileImage.clipsToBounds = true
        
        let imgUrl = URL(string:(userArray["image"] as! String))
        
        self.profileImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
        
        self.username.text = (userArray["username"] as! String)
        
        let city = ((userArray["location"] as! Dictionary<String, Any>)["city"] as! Dictionary<String, Any>)["name"] as! String
        let country = ((userArray["location"] as! Dictionary<String, Any>)["country"] as! Dictionary<String, Any>)["name"] as! String
        let region = ((userArray["location"] as! Dictionary<String, Any>)["region"] as! Dictionary<String, Any>)["name"] as! String
        
        self.location.text = city + ", " + region + ", " + country
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.aboutArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "aboutProfessionalCell", for: indexPath) as! AboutProfessionalCell
        
        let fixedWidth = cell.aboutText.frame.size.width
        
        let newSize = cell.aboutText.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        
        cell.aboutText.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height + 100)
        
        cell.aboutLabel.text = self.aboutArray[indexPath.row]["label"] as? String
        
        cell.aboutText.text = self.aboutArray[indexPath.row]["text"] as? String
        
        return cell
        
    }
    
}
