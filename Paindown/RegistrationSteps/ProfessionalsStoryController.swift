//
//  ProfessionalsStoryController.swift
//  Paindown
//
//  Created by James Suske on 2019-04-23.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

protocol ProfessionalsStoryDelegate: NSObjectProtocol {
    
    func professionalsStoryDelegateMethod(professional_stories: String)
    func professionalsStoryDelegateBackMethod()
    
}

class ProfessionalsStoryController: UIViewController {
    
    @IBOutlet var storyText: UITextView!

    var professional_stories = String()
    
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var previousButton: UIButton!
    
    var professionalsStoryDelegate: ProfessionalsStoryDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let borderColor = UIColor(displayP3Red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
        
        storyText.layer.borderColor = borderColor.cgColor;
        storyText.layer.borderWidth = 1.0
        storyText.layer.cornerRadius = 5.0
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
        professionalsStoryDelegate?.professionalsStoryDelegateMethod(professional_stories: storyText.text)
        
    }
    
    @IBAction func previousButtonPressed(_ sender: Any) {
        
        professionalsStoryDelegate?.professionalsStoryDelegateBackMethod()
        
    }
    
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
