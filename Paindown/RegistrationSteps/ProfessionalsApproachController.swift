//
//  ProfessionalsApproachController.swift
//  Paindown
//
//  Created by James Suske on 2019-04-23.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

protocol ProfessionalsApproachDelegate: NSObjectProtocol {
    
    func professionalsApproachDelegateMethod(professional_approach: String)
    func professionalsApproachDelegateBackMethod()
    
}

class ProfessionalsApproachController: UIViewController {
    
    @IBOutlet var approachText: UITextView!

    var professional_approach = String()

    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var previousButton: UIButton!
    
    var professionalsApproachDelegate: ProfessionalsApproachDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let borderColor = UIColor(displayP3Red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
        
        approachText.layer.borderColor = borderColor.cgColor;
        approachText.layer.borderWidth = 1.0
        approachText.layer.cornerRadius = 5.0
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
        professionalsApproachDelegate?.professionalsApproachDelegateMethod(professional_approach: approachText.text)
        
    }
    
    @IBAction func previousButtonPressed(_ sender: Any) {
        
        professionalsApproachDelegate?.professionalsApproachDelegateBackMethod()
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}
