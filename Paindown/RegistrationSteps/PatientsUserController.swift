//
//  PatientUserController.swift
//  Paindown
//
//  Created by James Suske on 2019-04-21.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit
import Firebase

protocol PatientsUserDelegate: NSObjectProtocol {
    
    func patientsUserDelegateMethod(username: String, email: String, password: String, imageUrl: String)
    
}

class PatientsUserController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet var username: UITextField!
    
    @IBOutlet var email: UITextField!
    
    @IBOutlet var password: UITextField!
    
    @IBOutlet var passwordConfirm: UITextField!
    
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var userImage: UIImageView!
    
    var imageUrl = String()
    
    var patientsUserDelegate: PatientsUserDelegate?
    
    var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        username.delegate = self
        
        email.delegate = self
        
        password.delegate = self
        
        passwordConfirm.delegate = self
        
        userImage.isUserInteractionEnabled = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        
        tapGestureRecognizer.numberOfTapsRequired = 1
        
        userImage.addGestureRecognizer(tapGestureRecognizer)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField == username)
        {
            email.becomeFirstResponder()
        }
        
        if(textField == email)
        {
            password.becomeFirstResponder()
        }
        
        if(textField == password)
        {
            passwordConfirm.becomeFirstResponder()
        }
        
        if(textField == passwordConfirm)
        {
            passwordConfirm.resignFirstResponder()
            
        }
        
        return false
        
    }
    
    func camera()
    {
        
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = .camera
        self.present(myPickerController, animated: true, completion: nil)
        
    }
    
    func photoLibrary()
    {
        
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = .photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true)
        
        self.createIndicator()
        
        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
        
        guard let url = info[UIImagePickerController.InfoKey.imageURL] as? URL else {
            
            return
            
        }
        
        let filename = String(url.lastPathComponent.split(separator: ".")[0]) as String
        
        let storage = Storage.storage(url:"gs://goalie-tracker-1548449985549.appspot.com/")
        
        var data = Data()
        
        data = image.pngData()!
        
        let storageRef = storage.reference()
        
        let imageRef = storageRef.child("images/" + filename + ".png")
        
        let _ = imageRef.putData(data, metadata: nil) { (metadata, error) in
            
            if(error != nil) {
                self.stopIndicator()
                self.customAlert(title: "Error", message: "Something went wrong uploading the image.")
            } else {
                imageRef.downloadURL { url, error2 in
                    self.imageUrl = url!.absoluteString
                    self.stopIndicator()
                    self.customAlert(title: "Success", message: "Image Uploaded")
                    
                }
            }
            
        }
        
        print(image.size)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
        if(self.username.text == "")
        {
            self.customAlert(title: "Error", message: "Username can't be blank.")
            return
        }
        
        if(self.email.text == "")
        {
            self.customAlert(title: "Error", message: "Email can't be blank.")
            return
        }
        
        if(self.password.text == "")
        {
            self.customAlert(title: "Error", message: "Password can't be blank.")
            return
        }
        
        if(self.passwordConfirm.text == "")
        {
            self.customAlert(title: "Error", message: "Password Confirm can't be blank.")
            return
        }
        
        if(self.password.text != self.passwordConfirm.text)
        {
            self.customAlert(title: "Error", message: "Password does not match.")
            return
        }
        
        checkByUsername(username: self.username.text!){ result in
            
            if((result["error"]) != nil)
            {
             
                self.checkByEmail(email: self.email.text!){ result in
                    
                    if((result["error"]) != nil)
                    {
                        self.patientsUserDelegate?.patientsUserDelegateMethod(username: self.username.text!, email: self.email.text!, password: self.password.text!, imageUrl: self.imageUrl)
                    }
                    else
                    {
                        self.customAlert(title: "Error", message: "Email has been found, please try another email.")
                        return
                    }
                    
                }
                
            }
            else
            {
                self.customAlert(title: "Error", message: "Username has been found, please try another username.")
                return
            }
            
        }
        
    }
    
    func checkByUsername(username: String, completionHandler:@escaping (_ result: Dictionary<String, Any>) -> Void)
    {
        var returnedResults = Dictionary<String, Any>()
        
        APIController().getUserByUsername(username: username)
        {
            (result: Dictionary<String, Any>) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
    func checkByEmail(email: String, completionHandler:@escaping (_ result: Dictionary<String, Any>) -> Void)
    {
        var returnedResults = Dictionary<String, Any>()
        
        APIController().getUserByEmail(email: email)
        {
            (result: Dictionary<String, Any>) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
    func createIndicator()
    {
        
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100)) as UIActivityIndicatorView
        
        indicator.center = self.view.center
        
        indicator.hidesWhenStopped = true
        
        indicator.style = UIActivityIndicatorView.Style.white
        
        indicator.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        
        indicator.layer.cornerRadius = 15
        
        self.view.addSubview(indicator)
        
        self.indicator.startAnimating()
    }
    
    func stopIndicator()
    {
        DispatchQueue.main.async {
            
            self.indicator.stopAnimating()
            self.indicator.removeFromSuperview()
            
        }
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
