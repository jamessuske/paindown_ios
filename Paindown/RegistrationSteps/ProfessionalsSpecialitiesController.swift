//
//  ProfessionalsSpecialitiesController.swift
//  Paindown
//
//  Created by James Suske on 2019-04-23.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

protocol ProfessionalsSpecialitiesDelegate: NSObjectProtocol {
    
    func professionalsSpecialitiesDelegateMethod(fkspecialities: Array<Dictionary<String, Any>>)
    func professionalsSpecialitiesDelegateBackMethod()
    
}

class ProfessionalsSpecialitiesController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var previousButton: UIButton!
    
    var professionalsSpecialitiesDelegate: ProfessionalsSpecialitiesDelegate?
    
    @IBOutlet var specialitiesAll: UITableView!
    
    @IBOutlet var specialitiesAdded: UITableView!
    
    var allArray = Array<Dictionary<String, Any>>()
    
    var addedArray = Array<Dictionary<String, Any>>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        specialitiesAll.delegate = self
        specialitiesAdded.delegate = self
        specialitiesAll.dataSource = self
        specialitiesAdded.dataSource = self
        
        getSpecialities(){ result in
            
            print(result)
            
            self.allArray = result
            
            self.specialitiesAll.reloadData()
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == specialitiesAll)
        {
            return self.allArray.count
        }
        else
        {
            return self.addedArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == specialitiesAll)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SpecialitiesAllCell", for: indexPath)
            
            cell.textLabel?.numberOfLines = 0
            
            cell.textLabel?.lineBreakMode = .byWordWrapping
            
            cell.textLabel?.font = .systemFont(ofSize: 12)
            
            cell.textLabel?.text = self.allArray[indexPath.row]["text"] as? String
            
            return cell
            
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SpecialitiesAddedCell", for: indexPath)
            
            cell.textLabel?.numberOfLines = 0
            
            cell.textLabel?.lineBreakMode = .byWordWrapping
            
            cell.textLabel?.font = .systemFont(ofSize: 12)
            
            cell.textLabel?.text = self.addedArray[indexPath.row]["text"] as? String
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(tableView == specialitiesAll)
        {
            
            self.addedArray.append(self.allArray[indexPath.row])
            self.allArray.remove(at: indexPath.row)
            
        }
        else
        {
            self.allArray.append(self.addedArray[indexPath.row])
            self.addedArray.remove(at: indexPath.row)
        }
        
        specialitiesAll.reloadData()
        specialitiesAdded.reloadData()
        
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
        professionalsSpecialitiesDelegate?.professionalsSpecialitiesDelegateMethod(fkspecialities: self.addedArray)
        
    }
    
    @IBAction func previousButtonPressed(_ sender: Any) {
        
        professionalsSpecialitiesDelegate?.professionalsSpecialitiesDelegateBackMethod()
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func getSpecialities(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getSpecialities()
        {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
}
