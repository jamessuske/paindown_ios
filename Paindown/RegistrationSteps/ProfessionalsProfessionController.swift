//
//  ProfessionalsProfessionController.swift
//  Paindown
//
//  Created by James Suske on 2019-04-23.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

protocol ProfessionalsProfessionDelegate: NSObjectProtocol {
    
    func professionalsProfessionDelegateMethod(fkprofession: String, professional_website: String)
    func professionalsProfessionDelegateBackMethod()
    
}

class ProfessionalsProfessionController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet var professionText: UITextField!
    
    @IBOutlet var websiteText: UITextField!
    
    let pickerView = UIPickerView()
    
    var pickOption = Array<Dictionary<String, Any>>()
    
    var fkprofession = String()
    
    var professional_website = String()
    
    var professionalsProfessionDelegate: ProfessionalsProfessionDelegate?
    
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var previousButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let professionGesture = UITapGestureRecognizer(target: self, action: #selector(professionTapped))
        
        professionText.addGestureRecognizer(professionGesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getProfession(){ result in
            
            self.pickOption = result
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]["text"] as? String
    }
    
    @objc func professionTapped() {
        
        pickerView.delegate = self
        
        pickerView.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(ProfessionalsProfessionController.donePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(ProfessionalsProfessionController.cancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        professionText.inputAccessoryView = toolBar
        
        view.addSubview(professionText)
        
        professionText.inputView = pickerView
        
        professionText.becomeFirstResponder()
        
        
    }
    
    @objc func donePicker() {
        
        professionText.resignFirstResponder()
        
        self.professionText.text = pickOption[pickerView.selectedRow(inComponent: 0)]["text"] as? String
        
        fkprofession = String(pickOption[pickerView.selectedRow(inComponent: 0)]["id"] as! Int)
        
    }
    
    @objc func cancelPicker() {
        
        professionText.resignFirstResponder()
        
    }
    
    @IBAction func previousButtonPressed(_ sender: Any) {
        
        self.professionalsProfessionDelegate?.professionalsProfessionDelegateBackMethod()
        
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
        self.professionalsProfessionDelegate?.professionalsProfessionDelegateMethod(fkprofession: self.fkprofession, professional_website: self.websiteText.text!)
        
    }
    
    func getProfession(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getProfessions()
        {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
