//
//  ProfessionalsAboutController.swift
//  Paindown
//
//  Created by James Suske on 2019-04-23.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

protocol ProfessionalsAboutDelegate: NSObjectProtocol {
    
    func professionalsAboutDelegateMethod(professional_about: String)
    func professionalsAboutDelegateBackMethod()
    
}

class ProfessionalsAboutController: UIViewController {
    
    @IBOutlet var aboutText: UITextView!
    
    var professional_about = String()
    
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var previousButton: UIButton!
    
    var professionalsAboutDelegate: ProfessionalsAboutDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let borderColor = UIColor(displayP3Red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
        
        aboutText.layer.borderColor = borderColor.cgColor;
        aboutText.layer.borderWidth = 1.0
        aboutText.layer.cornerRadius = 5.0
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
        print(aboutText.text!)
        
        professionalsAboutDelegate?.professionalsAboutDelegateMethod(professional_about: aboutText.text)
        
    }
    
    @IBAction func previousButtonPressed(_ sender: Any) {
        
        professionalsAboutDelegate?.professionalsAboutDelegateBackMethod()
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
}
