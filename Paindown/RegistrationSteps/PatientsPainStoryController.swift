//
//  PatientsPainStoryController.swift
//  Paindown
//
//  Created by James Suske on 2019-04-23.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

protocol PatientsPainStoryDelegate: NSObjectProtocol {
    
    func patientsPainStoryDelegateMethod(is_public: Bool, patient_painstory: String)
    func patientsPainStoryBackDelegateMethod()
    
}

class PatientsPainStoryController: UIViewController {
    
    @IBOutlet var publicStory: UISwitch!
    
    @IBOutlet var story: UITextView!
    
    @IBOutlet var submitButton: UIButton!
    
    @IBOutlet var previousButton: UIButton!
    
    var is_public = Bool()
    
    var patient_painstory = String()
    
    var patientsPainStoryDelegate: PatientsPainStoryDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let borderColor = UIColor(displayP3Red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
        
        story.layer.borderColor = borderColor.cgColor;
        story.layer.borderWidth = 1.0
        story.layer.cornerRadius = 5.0
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func publicStorySwitch(_ sender: Any) {
    }
    
    @IBAction func previousButtonPressed(_ sender: Any) {
        
        patientsPainStoryDelegate?.patientsPainStoryBackDelegateMethod()
        
    }
    
    @IBAction func submitButtonPressed(_ sender: Any) {
        
        patientsPainStoryDelegate?.patientsPainStoryDelegateMethod(is_public: publicStory.isOn, patient_painstory: story.text)
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
}
