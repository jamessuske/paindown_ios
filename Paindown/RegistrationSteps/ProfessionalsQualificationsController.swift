//
//  ProfessionalsQualificationsController.swift
//  Paindown
//
//  Created by James Suske on 2019-04-23.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

protocol ProfessionalsQualificationsDelegate: NSObjectProtocol {
    
    func professionalsQualificationsDelegateMethod(fkqualifications: Array<Dictionary<String, Any>>)
    func professionalsQualificationsDelegateBackMethod()
    
}

class ProfessionalsQualificationsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var submitButton: UIButton!
   
    @IBOutlet var previousButton: UIButton!
    
    var professionalsQualificationsDelegate: ProfessionalsQualificationsDelegate?
    
    @IBOutlet var qualificationsAll: UITableView!
    
    @IBOutlet var qualificationsAdded: UITableView!
    
    var allArray = Array<Dictionary<String, Any>>()
    
    var addedArray = Array<Dictionary<String, Any>>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        qualificationsAll.delegate = self
        qualificationsAdded.delegate = self
        qualificationsAll.dataSource = self
        qualificationsAdded.dataSource = self
        
        getQualifications(){ result in
            
            print(result)
            
            self.allArray = result
            
            self.qualificationsAll.reloadData()
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == qualificationsAll)
        {
            return self.allArray.count
        }
        else
        {
            return self.addedArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == qualificationsAll)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QualificationAllCell", for: indexPath)
            
            cell.textLabel?.numberOfLines = 0
            
            cell.textLabel?.lineBreakMode = .byWordWrapping
            
            cell.textLabel?.font = .systemFont(ofSize: 12)
            
            cell.textLabel?.text = self.allArray[indexPath.row]["text"] as? String
            
            return cell
            
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QualificationAddedCell", for: indexPath)
            
            cell.textLabel?.numberOfLines = 0
            
            cell.textLabel?.lineBreakMode = .byWordWrapping
            
            cell.textLabel?.font = .systemFont(ofSize: 12)
            
            cell.textLabel?.text = self.addedArray[indexPath.row]["text"] as? String
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(tableView == qualificationsAll)
        {
            
            self.addedArray.append(self.allArray[indexPath.row])
            self.allArray.remove(at: indexPath.row)
            
        }
        else
        {
            self.allArray.append(self.addedArray[indexPath.row])
            self.addedArray.remove(at: indexPath.row)
        }
        
        qualificationsAll.reloadData()
        qualificationsAdded.reloadData()
        
    }
    
    @IBAction func submitButtonPressed(_ sender: Any) {
        
        professionalsQualificationsDelegate?.professionalsQualificationsDelegateMethod(fkqualifications: self.addedArray)
        
    }
    
    @IBAction func previousButtonPressed(_ sender: Any) {
        
        professionalsQualificationsDelegate?.professionalsQualificationsDelegateBackMethod()
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func getQualifications(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getQualifications()
        {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
}
