//
//  BlogPostController.swift
//  Paindown
//
//  Created by James Suske on 2018-09-11.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage

class BlogPostController: UIViewController {
    
    @IBOutlet var postImage: UIImageView!
    
    @IBOutlet var postTitle: UILabel!
    
    @IBOutlet var postText: UITextView!
    
    var postid = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getBlogPost(postid: self.postid){ result in
         
            self.postTitle.text = (result["title"] as! Dictionary<String, String>)["rendered"]! as String
            
            self.postText.text = ((result["content"] as! Dictionary<String, Any>)["rendered"]! as! String).replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
            
            let imageUrl = URL(string: (result["better_featured_image"] as! Dictionary<String, Any>)["source_url"] as! String)
            
            self.postImage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "default.png"))
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getBlogPost(postid: String, completionHandler:@escaping (_ result:Dictionary<String, Any>) -> Void)
    {
        var returnedResults = Dictionary<String, Any>()
        
        APIController().getBlogPost(postid: postid)
        {
                (result: Dictionary<String, Any>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
}
