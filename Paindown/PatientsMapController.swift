//
//  PatientsMapController.swift
//  Paindown
//
//  Created by James Suske on 2019-04-15.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit
import GoogleMaps
import SideMenu

class PatientsMapController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, PatientsFilterControllerDelegate {
    
    let locationManager = CLLocationManager()
    
    @IBOutlet var filterButton: UIBarButtonItem!
    
    @IBOutlet var listButton: UIBarButtonItem!
    
    var mapView = GMSMapView()
    
    var patients = Array<Dictionary<String, Any>>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: 22.300000, longitude: 70.783300, zoom: 10.0)
        mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        mapView.mapType = .terrain
        mapView.camera = camera
        mapView.delegate = self
        self.view = mapView
        
        getPatients(){ result in
            
            self.patients = result
            
            self.locationManager.requestAlwaysAuthorization()
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                self.locationManager.delegate = self
                self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                self.locationManager.startUpdatingLocation()
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        let newcamera = GMSCameraPosition.camera(withLatitude: (locValue.latitude), longitude: (locValue.longitude), zoom: 13.0)
        
        self.mapView.animate(to: newcamera)
        
        for patient in self.patients
        {
            
            let city = (patient["location"] as! Dictionary<String, Dictionary<String, Any>>)["city"]!["name"] as! String
            let region = (patient["location"] as! Dictionary<String, Dictionary<String, Any>>)["region"]!["name"] as! String
            let country = (patient["location"] as! Dictionary<String, Dictionary<String, Any>>)["country"]!["name"] as! String
            
            if((patient["position"] as! Dictionary<String, Any>)["lat"]! as! CLLocationDegrees != 0 && (patient["position"] as! Dictionary<String, Any>)["long"]! as! CLLocationDegrees != 0)
            {
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: (patient["position"] as! Dictionary<String, Any>)["lat"]! as! CLLocationDegrees, longitude: (patient["position"] as! Dictionary<String, Any>)["long"]! as! CLLocationDegrees)
                marker.title = patient["username"] as? String
                marker.snippet = "Number of years in pain: " + (patient["pain_years"] as? String)! + "\n" + city + ", " + region + ", " + country
                marker.map = mapView
            }
            
        }
        
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "PatientProfileController") as! PatientProfileController
        
        viewController.username = marker.title!
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func listButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "PatientsController") as! PatientsController
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    
    
    func patientsFilterControllerDelegateMethod(order: String, location: String, disMin: String, disMax: String, painMin: String, painMax: String, pain: String, painkillers: String, lat: String, long: String) {
        
        getPatientsFilters(order: order, location: location, disMin: disMin, disMax: disMin, painMin: painMin, painMax: painMax, painKillers: painkillers, painType: pain, long: long, lat: lat){ result in

            
            self.mapView.clear()
            
            self.patients = result
            
            for patient in self.patients
            {
                
                //let city = (patient["location"] as! Dictionary<String, Dictionary<String, Any>>)["city"]!["name"] as! String
                //let region = (patient["location"] as! Dictionary<String, Dictionary<String, Any>>)["region"]!["name"] as! String
                //let country = (patient["location"] as! Dictionary<String, Dictionary<String, Any>>)["country"]!["name"] as! String
                
                if((patient["position"] as! Dictionary<String, Any>)["lat"]! as! CLLocationDegrees != 0 && (patient["position"] as! Dictionary<String, Any>)["long"]! as! CLLocationDegrees != 0)
                {
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2D(latitude: (patient["position"] as! Dictionary<String, Any>)["lat"]! as! CLLocationDegrees, longitude: (patient["position"] as! Dictionary<String, Any>)["long"]! as! CLLocationDegrees)
                    marker.title = patient["username"] as? String
                    //marker.snippet = "Number of years in pain: " + (patient["pain_years"] as? String)! + "\n" + city + ", " + region + ", " + country
                    marker.snippet = "Number of years in pain: " + (patient["pain_years"] as? String)!
                    marker.map = self.mapView
                }
                
            }
            
        }
        
    }
    
    @IBAction func filterButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "PatientsFilterController") as! PatientsFilterController
        
        viewController.patientsFilterControllerDelegate = self
        
        let menuRightNavigationController = UISideMenuNavigationController(rootViewController: viewController)
        
        SideMenuManager.default.menuRightNavigationController = menuRightNavigationController
        
        present(menuRightNavigationController, animated: true, completion: nil)
        
    }
    
    func getPatientsFilters(order: String, location: String, disMin: String, disMax: String, painMin: String, painMax: String, painKillers: String, painType: String, long: String, lat: String, completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getPatientsFilters(painType: painType, location: location, order: order, disMin: disMin, disMax: disMax, painMin: painMin, painMax: painMax, painKillers: painKillers, long: long, lat: lat)
        {
            (result: Array<Dictionary<String, Any>>) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
    func getPatients(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getPatients()
            {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    
}
