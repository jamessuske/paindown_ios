//
//  UsersPasswordClass.swift
//  Paindown
//
//  Created by James Suske on 2019-07-27.
//  Copyright © 2019 James Suske. All rights reserved.
//

import Foundation

class UsersPasswordClass: NSObject {

    var email: String
    
    var password: String
    
    var newpassword: String
    
    var password_confirmation: String
    
    init(email: String, password: String, newpassword: String, password_confirmation: String) {
        
        self.email = email
        
        self.password = password
        
        self.newpassword = newpassword
        
        self.password_confirmation = password_confirmation
        
    }
    
}
