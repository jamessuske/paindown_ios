//
//  ProfessionalRegisterClass.swift
//  Paindown
//
//  Created by James Suske on 2019-04-17.
//  Copyright © 2019 James Suske. All rights reserved.
//

import Foundation

class ProfessionalRegisterClass: NSObject {
    
    var professional_name = String()
    
    var professional_username = String()
    
    var professional_phone = String()
    
    var email = String()
    
    var password = String()
    
    var fkcountry = Int()
    
    var fkregion = Int()
    
    var fkcity = Int()
    
    var professional_address = String()
    
    var fkprofession = String()
    
    var professional_website = String()
    
    var professional_about = String()
    
    var professional_approach = String()
    
    var professional_stories = String()
    
    var fkspecialities = Array<Dictionary<String, Any>>()
    
    var fkqualifications = Array<Dictionary<String, Any>>()
    
    var professional_image = String()
    
    var city_name = String()
    
    override init()
    {
        
    }
    
}
