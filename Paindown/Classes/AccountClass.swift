//
//  AccountClass.swift
//  Paindown
//
//  Created by James Suske on 2019-06-16.
//  Copyright © 2019 James Suske. All rights reserved.
//

import Foundation

class AccountClass: NSObject {
    
    var title: String
    var controller: String
    
    init(title: String, controller: String) {
        
        self.title = title
        
        self.controller = controller
        
    }
    
}
