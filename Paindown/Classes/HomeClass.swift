//
//  HomeClass.swift
//  Paindown
//
//  Created by James Suske on 2019-06-16.
//  Copyright © 2019 James Suske. All rights reserved.
//

import Foundation

class HomeClass: NSObject {
    
    var icon: String
    var title: String
    var controller: String
    
    init(icon: String, title: String, controller: String) {
        
        self.icon = icon
        
        self.title = title
        
        self.controller = controller
        
    }
    
}
