//
//  PatientsRegisterClass.swift
//  Paindown
//
//  Created by James Suske on 2019-04-16.
//  Copyright © 2019 James Suske. All rights reserved.
//

import Foundation

class PatientsRegisterClass: NSObject {
    
    var user_name = String()
    
    var email = String()
    
    var password = String()
    
    var fkcountry = Int()
    
    var fkregion = Int()
    
    var fkcity = Int()
    
    var address = String()
    
    var fkpaintype = String()
    
    var fkpainlevel = String()
    
    var patient_painyears = Int()
    
    var usage_painkillers = Bool()
    
    var painkiller = Int()
    
    var other_painkiller = String()
    
    var is_public = Bool()
    
    var patient_painstory = String()
    
    var patient_image = String()
    
    var city_name = String()
    
    override init()
    {
        
    }
    
}
