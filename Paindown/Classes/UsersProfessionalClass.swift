//
//  UsersClass.swift
//  Paindown
//
//  Created by James Suske on 2019-08-05.
//  Copyright © 2019 James Suske. All rights reserved.
//

import Foundation

class UsersProfessionalClass: NSObject {

    var about = String()
    
    var approach = String()
    
    var email = String()
    
    var image = String()
    
    var isPainkillers = Bool()
    
    var location = Dictionary<String, Any>()
    
    var membership = Int()
    
    var membership_date = String()
    
    var name = String()
    
    var phone = String()
    
    var preference = Array<Dictionary<String, Any>>()
    
    var profession = Int()
    
    var isPublic = Bool() //@TODO: isPublic to public in API Call
    
    var specialities = Array<Dictionary<String, Any>>()
    
    var qualifications = Array<Dictionary<String, Any>>()
    
    var stories = String()
    
    var user_name = String()
    
    var website = String()
    
    override init()
    {
        
    }
    
}
