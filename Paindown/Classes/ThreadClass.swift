//
//  ThreadClass.swift
//  Paindown
//
//  Created by James Suske on 2019-05-18.
//  Copyright © 2019 James Suske. All rights reserved.
//

import Foundation

class ThreadClass: NSObject {
    
    var body = String()
    
    var date = Date()
    
    var username = String()
    
    init(body: String, date: Date, username: String){
        
        self.body = body
        
        self.date = date
        
        self.username = username
        
    }
    
}
