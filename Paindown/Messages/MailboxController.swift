//
//  MailboxController.swift
//  Paindown
//
//  Created by James Suske on 2019-05-12.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class MailboxController: UITableViewController {
    
    var messages = Array<Dictionary<String, Any>>()
    
    var mailbox = String()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let backButton = UIBarButtonItem(image: UIImage(named: "imagename"), style: .plain, target: self, action: #selector(backButtonPressed))
        self.navigationItem.leftBarButtonItem  = backButton
        
        if(mailbox == "inbox")
        {
            self.getMessages(){ result in
                
                self.messages = result
                
                self.tableView.reloadData()
                
            }
        }
        else if(mailbox == "sent")
        {
            self.getSentMessages(){ result in
                
                self.messages = result
                
                print(result)
                
                self.tableView.reloadData()
                
            }
        }
        else
        {
            self.getDeletedMessages(){ result in
                
                self.messages = result
                
                print(result)
                
                self.tableView.reloadData()
                
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func backButtonPressed()
    {
        dismiss(animated: true, completion: nil)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "mailBoxCell", for: indexPath) as! MailboxCell
        
        cell.cellText.text = (self.messages[indexPath.row]["username"] as! String)
        cell.cellSubject.text = (self.messages[indexPath.row]["subject"] as! String)

        return cell

    }
    
    @IBAction func mailboxesButtonPressed(_ sender: Any) {
    
        dismiss(animated: true, completion: nil)
    
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "MessagesSegue" {
            
            if let indexPath = tableView.indexPathForSelectedRow {
                
                let object = (self.messages[indexPath.row]["id"] as! Int)
                
                let controller = (segue.destination as! UINavigationController).topViewController as! MessagesDetailController
                
                controller.detailItem = object
                
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                
                controller.navigationItem.leftItemsSupplementBackButton = true
                
            }
        }
    }
    
    func getMessages(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getMessages()
            {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    func getSentMessages(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getSentMessages()
            {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    func getDeletedMessages(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getDeletedMessages()
            {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
}
