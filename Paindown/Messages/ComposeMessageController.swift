//
//  ComposeMessageController.swift
//  Paindown
//
//  Created by James Suske on 2019-05-20.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class ComposeMessageController: UIViewController {
    
    @IBOutlet var message: UITextView!
    
    @IBOutlet var sendButton: UIButton!
    
    var username = String()
    
    var userid = Int()
    
    var threadId = Int()
    
    var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = .white
        
        let borderColor = UIColor(displayP3Red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
        
        message.layer.borderColor = borderColor.cgColor;
        message.layer.borderWidth = 1.0
        message.layer.cornerRadius = 5.0
        
        composeMessage(username: username){ result in
            
            self.userid = (result[0]["id"] as? Int)!
            
            print(result)
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        
        self.createIndicator()
        
        updateMessage(thread: self.threadId, body: self.message.text){ result in
         
            self.stopIndicator()
            self.customAlert(title: "Message Sent", message: "Your message has been sent.")
            
        }
        
    }
    
    func composeMessage(username: String, completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().composeMessage(username: username)
        {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    func postMessage(recipients: String, subject: String, message: String, completionHandler:@escaping (_ result:Dictionary<String, Any>) -> Void)
    {
        var returnedResults = Dictionary<String, Any>()
        
        APIController().postMessage(recipients: recipients, subject: subject, message: message)
        {
            (result: Dictionary<String, Any>) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
    func updateMessage(thread: Int, body: String, completionHandler:@escaping (_ result:Dictionary<String, Any>) -> Void)
    {
        var returnedResults = Dictionary<String, Any>()
        
        APIController().updateMessage(thread: thread, body: body)
        {
            (result: Dictionary<String, Any>) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
    func createIndicator()
    {
        
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100)) as UIActivityIndicatorView
        
        indicator.center = self.view.center
        
        indicator.hidesWhenStopped = true
        
        indicator.style = UIActivityIndicatorView.Style.white
        
        indicator.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        
        indicator.layer.cornerRadius = 15
        
        self.view.addSubview(indicator)
        
        self.indicator.startAnimating()
    }
    
    func stopIndicator()
    {
        DispatchQueue.main.async {
            
            self.indicator.stopAnimating()
            self.indicator.removeFromSuperview()
            
        }
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default){ action -> Void in
            
            self.navigationController!.dismiss(animated: true, completion: nil)
            
        })
        
        self.present(alertController, animated: true, completion: nil)
    }
}
