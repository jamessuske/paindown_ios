//
//  MessagesDetailController.swift
//  Paindown
//
//  Created by James Suske on 2019-04-21.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class MessagesDetailController: UICollectionViewController {
    
    var messages = Array<Dictionary<String, Any>>()
    
    var thread = Dictionary<String, Any>()
    
    var threadId = Int()

    @IBOutlet var replyButton: UIBarButtonItem!
    
    func configureView() {
        if let detail = detailItem {

            threadId = detail
            
            getTheards(id: String(detail)) { result in

                self.messages = result["messages"] as! Array<Dictionary<String, Any>>
 
                self.thread = result["thread"] as! Dictionary<String, Any>
                
                self.collectionView.reloadData()
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = .white
        
        configureView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    @IBAction func replyButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "ComposeMessageController") as! ComposeMessageController
        
        viewController.threadId = self.threadId
        
        self.navigationController?.pushViewController(viewController, animated: true)
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ComposeMessageSegue" {
            
            let controller = (segue.destination as! ComposeMessageController)
            
            controller.username = "jsuske"
            
            controller.threadId = self.threadId
            
            controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
            
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }
    
     override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "messagesCell", for: indexPath) as! MessagesCell
        
        let fixedWidth = cell.cellText.frame.size.width
        
        let newSize = cell.cellText.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        
        cell.cellText.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height + 100)
        
        cell.cellText.text = (self.messages[indexPath.row]["body"] as! String)
        
        cell.from.text = "From: " + (self.messages[indexPath.row]["username"] as! String)
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let messaageDate = dateFormatter.date(from: (self.messages[indexPath.row]["updated_at"] as! String))
        
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        
        cell.date.text = dateFormatter.string(from: messaageDate!)
        
        return cell
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "MessagesCellHeader", for: indexPath) as! MessagesCellHeader
        
        if let subject = self.thread["subject"] as? String{
            
            headerView.subject.text = "Subject: " + subject
            
        }
        
        return headerView
        
    }
    
    var detailItem: Int? {
        didSet {
            configureView()
        }
    }
    
    func getTheards(id: String, completionHandler:@escaping (_ result:Dictionary<String, Any>) -> Void)
    {
        var returnedResults = Dictionary<String, Any>()
        
        APIController().getThread(id: id)
        {
                (result: Dictionary<String, Any>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
}
