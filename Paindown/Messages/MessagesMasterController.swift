//
//  MessagesController.swift
//  Paindown
//
//  Created by James Suske on 2019-03-26.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit
import SideMenu

class MessagesMasterController: UITableViewController {
    
    var messages = Array<Dictionary<String, Any>>()
    
    @IBOutlet var menuButton: UIBarButtonItem!
    
    @IBOutlet var segments: UISegmentedControl!
    
    var mailboxes = Array<String>()
    
    @IBOutlet var homeButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = .white
        
        mailboxes = ["inbox", "sent", "deleted"]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mailboxes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "messagesCell", for: indexPath)
        
        cell.textLabel?.text = self.mailboxes[indexPath.row].capitalized
        
        cell.imageView?.image = UIImage(named: self.mailboxes[indexPath.row])
        
        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "MailboxSegue" {

            if let indexPath = tableView.indexPathForSelectedRow {
  
                let object = (self.mailboxes[indexPath.row])

                let controller = (segue.destination as! UINavigationController).topViewController as! MailboxController

                controller.mailbox = object
                
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem

                controller.navigationItem.leftItemsSupplementBackButton = true
                
            }
        }
    }
    
    @IBAction func homeButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
