//
//  AccountController.swift
//  Paindown
//
//  Created by James Suske on 2018-08-31.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit
import SDWebImage

class AccountController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var username: UILabel!
    
    @IBOutlet var accountCollection: UICollectionView!
    
    var accountArray = Array<AccountClass>()
    
    var userProfile = Dictionary<String, Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(UserDefaults.standard.string(forKey: "type") == "professional")
        {
            accountArray.append(AccountClass(title: "Personal", controller: "AccountProfessional"))
        }
        else
        {
            accountArray.append(AccountClass(title: "Personal", controller: "AccountPatient"))
        }

        accountArray.append(AccountClass(title: "Password", controller: "AccountPassword"))
        accountArray.append(AccountClass(title: "Advanced", controller: "AccountAdvanced"))
        accountArray.append(AccountClass(title: "Subscription", controller: "AccountSubscription"))
        
        accountCollection.delegate = self
        accountCollection.dataSource = self
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = (profileImage.frame.height) / 2
        profileImage.clipsToBounds = true
        
        getUserProfile(){ result in
            
            print(result)
            
            self.userProfile = result
            
            if(result["image"] is NSNull)
            {
                self.profileImage.image = UIImage(named: "default.png")
            }
            else
            {
                let imgUrl = URL(string:(result["image"] as! String))
                self.profileImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
            }
            
            self.username.text = (result["username"] as! String)
        
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.accountArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AccountCell", for: indexPath) as! AccountCell
        
        cell.item.text = self.accountArray[indexPath.row].title
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(indexPath.row == 0)
        {
            
            if(self.accountArray[indexPath.row].controller == "AccountProfessional")
            {
                let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountProfessional") as! AccountProfessional
                
                viewController.userProfile = self.userProfile
                
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            else
            {
                let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountPatients") as! AccountPatients
                
                viewController.userProfile = self.userProfile
                
                self.navigationController?.pushViewController(viewController, animated: true)
            }

        }
        else if(indexPath.row == 1)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountPassword") as! AccountPassword
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 2)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountAdvanced") as! AccountAdvanced
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(indexPath.row == 3)
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "AccountSubscription") as! AccountSubscription
            
            viewController.userProfile = self.userProfile
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
    func getUserProfile(completionHandler:@escaping (_ result:Dictionary<String, Any>) -> Void)
    {
        var returnedResults = Dictionary<String, Any>()
        
        APIController().getUserProfile()
        {
            (result: Dictionary<String, Any>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
}
