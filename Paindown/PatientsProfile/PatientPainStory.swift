//
//  PatientPainStory.swift
//  Paindown
//
//  Created by James Suske on 2019-06-17.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class PatientPainStory: UIViewController {
    
    var userArray = Dictionary<String, Any>()
    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var username: UILabel!
    
    @IBOutlet var location: UILabel!
    
    @IBOutlet var painStory: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = (profileImage.frame.height) / 2
        profileImage.clipsToBounds = true
        
        painStory.text = userArray["story"] as? String
        
        let imgUrl = URL(string:(userArray["image"] as! String))
        
        self.profileImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
        
        self.username.text = (userArray["username"] as! String)
        
        let city = ((userArray["location"] as! Dictionary<String, Any>)["city"] as! Dictionary<String, Any>)["name"] as! String
        let country = ((userArray["location"] as! Dictionary<String, Any>)["country"] as! Dictionary<String, Any>)["name"] as! String
        let region = ((userArray["location"] as! Dictionary<String, Any>)["region"] as! Dictionary<String, Any>)["name"] as! String
        
        self.location.text = city + ", " + region + ", " + country
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
