//
//  PatientPainkillers.swift
//  Paindown
//
//  Created by James Suske on 2019-06-17.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class PatientPainkillers: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var userArray = Dictionary<String, Any>()
    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var username: UILabel!
    
    @IBOutlet var location: UILabel!
    
    @IBOutlet var painKillersCollection: UICollectionView!
    
    var painKillersArray = Array<Dictionary<String, String>>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        painKillersCollection.delegate = self
        painKillersCollection.dataSource = self
        
        painKillersArray.append(["label" : "Painkillers", "text" : userArray["patient_painkiller"] as! String])
        painKillersArray.append(["label" : "Other Painkillers", "text" : userArray["other_painkiller"] as! String])
        
        painKillersCollection.reloadData()
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = (profileImage.frame.height) / 2
        profileImage.clipsToBounds = true
        
        let imgUrl = URL(string:(userArray["image"] as! String))
        
        self.profileImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
        
        self.username.text = (userArray["username"] as! String)
        
        let city = ((userArray["location"] as! Dictionary<String, Any>)["city"] as! Dictionary<String, Any>)["name"] as! String
        let country = ((userArray["location"] as! Dictionary<String, Any>)["country"] as! Dictionary<String, Any>)["name"] as! String
        let region = ((userArray["location"] as! Dictionary<String, Any>)["region"] as! Dictionary<String, Any>)["name"] as! String
        
        self.location.text = city + ", " + region + ", " + country
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.painKillersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "painkillerCell", for: indexPath) as! PainkillersCell
        
        cell.painKillerLabel.text = self.painKillersArray[indexPath.row]["label"]
        
        cell.painKillerText.text = self.painKillersArray[indexPath.row]["text"]
        
        return cell
        
    }
    
}
