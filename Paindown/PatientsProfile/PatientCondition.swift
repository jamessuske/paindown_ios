//
//  PatientCondition.swift
//  Paindown
//
//  Created by James Suske on 2019-06-17.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class PatientCondition: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var userArray = Dictionary<String, Any>()
    
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var username: UILabel!
    
    @IBOutlet var location: UILabel!
    
    @IBOutlet var conditionsCollection: UICollectionView!
    
    var conditionsArray = Array<Dictionary<String, String>>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        conditionsArray.append(["label" : "Type of condition", "text" : (userArray["pain_type"] as! Dictionary<String, Any>)["paintype_name"]! as! String])
        conditionsArray.append(["label" : "Level of condition", "text" : (userArray["pain_level"] as! Dictionary<String, Any>)["painlevel_name"]! as! String])
        conditionsArray.append(["label" : "Number of years with condition", "text" : userArray["pain_years"]! as! String])

        conditionsCollection.reloadData()
        
        conditionsCollection.delegate = self
        conditionsCollection.dataSource = self
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = (profileImage.frame.height) / 2
        profileImage.clipsToBounds = true
        
        let imgUrl = URL(string:(userArray["image"] as! String))
        
        self.profileImage.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default.png"))
        
        self.username.text = (userArray["username"] as! String)
        
        let city = ((userArray["location"] as! Dictionary<String, Any>)["city"] as! Dictionary<String, Any>)["name"] as! String
        let country = ((userArray["location"] as! Dictionary<String, Any>)["country"] as! Dictionary<String, Any>)["name"] as! String
        let region = ((userArray["location"] as! Dictionary<String, Any>)["region"] as! Dictionary<String, Any>)["name"] as! String
        
        self.location.text = city + ", " + region + ", " + country
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.conditionsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "conditionCell", for: indexPath) as! ConditionCell
        
        cell.conditionType.text = self.conditionsArray[indexPath.row]["label"]
        
        cell.conditionText.text = self.conditionsArray[indexPath.row]["text"]
        
        return cell
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
