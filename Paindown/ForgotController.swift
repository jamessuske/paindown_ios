//
//  ForgetController.swift
//  Paindown
//
//  Created by James Suske on 2018-07-23.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit

class ForgotController: UIViewController {
    
    @IBOutlet var email: UITextField!
    
    @IBOutlet var submitButton: UIButton!
    
    var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.view.backgroundColor = .clear
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func submitButtonPressed(_ sender: Any) {
        
        self.createIndicator()
        
        forgotPassword(email: email.text!){ result in
            
            if(result == "")
            {
                self.customAlert(title: "Error", message: "The email address enter is not assciated with an account.")
            }
            else
            {
                self.customAlert(title: "Success", message: result)
            }
            
            self.stopIndicator()
            
        }
        
    }
    
    func forgotPassword(email: String, completionHandler:@escaping (_ result: String) -> Void)
    {
        var returnedResults = String()
        
        APIController().forgotPassword(email: email)
        {
                (result: String) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    func createIndicator()
    {
        
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100)) as UIActivityIndicatorView
        
        indicator.center = self.view.center
        
        indicator.hidesWhenStopped = true
        
        indicator.style = UIActivityIndicatorView.Style.white
        
        indicator.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        
        indicator.layer.cornerRadius = 15
        
        self.view.addSubview(indicator)
        
        self.indicator.startAnimating()
    }
    
    func stopIndicator()
    {
        DispatchQueue.main.async {
            
            self.indicator.stopAnimating()
            self.indicator.removeFromSuperview()
            
        }
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
