//
//  BlogController.swift
//  Paindown
//
//  Created by James Suske on 2018-09-11.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage

class BlogController: UICollectionViewController {
    
    var posts = Array<Dictionary<String, Any>>()
    
    @IBOutlet var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getBlog(){ result in
            
            self.posts = result
            
            self.collectionView.reloadData()
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "blogCell", for: indexPath) as! BlogCell
        
        cell.cellText.text = (self.posts[indexPath.row]["title"] as! Dictionary<String, String>)["rendered"]! as String
        
        cell.postid = String(self.posts[indexPath.row]["id"] as! Int)
        
        cell.cellImage?.layer.borderWidth = 1
        cell.cellImage?.layer.masksToBounds = false
        cell.cellImage?.layer.borderColor = UIColor.clear.cgColor
        cell.cellImage?.layer.cornerRadius = (cell.cellImage?.frame.height)! / 2
        cell.cellImage?.clipsToBounds = true
        
        print((self.posts[indexPath.row]["better_featured_image"] as! Dictionary<String, Any>)["source_url"] as! String)
        
        let imageUrl = URL(string: (self.posts[indexPath.row]["better_featured_image"] as! Dictionary<String, Any>)["source_url"] as! String)
        
        cell.cellImage?.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "default.png"))
        
        return cell
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "BlogPost") as! BlogPostController
        
        viewController.postid = String(self.posts[indexPath.row]["id"] as! Int)
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 115)
    }
    
    @IBAction func menuButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "MenuController") as! MenuController
        
        let menuRightNavigationController = UISideMenuNavigationController(rootViewController: viewController)
        
        SideMenuManager.default.menuRightNavigationController = menuRightNavigationController
        
        present(menuRightNavigationController, animated: true, completion: nil)
        
    }
    
    func getBlog(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getBlog()
            {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
}
