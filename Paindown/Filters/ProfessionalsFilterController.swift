//
//  FilterController.swift
//  Paindown
//
//  Created by James Suske on 2019-04-17.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit
import RangeSeekSlider
import CoreLocation

protocol ProfessionalsFilterControllerDelegate: NSObjectProtocol {
    
    func professionalsFilterControllerDelegateMethod(order: String, location: String, disMin: String, disMax: String, pros: String, spec: String, lat: String, long: String)
    
}

class ProfessionalsFilterController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    
    @IBOutlet var closeButton: UIBarButtonItem!
    
    @IBOutlet var orderTextField: UITextField!
    
    @IBOutlet var locationTextField: UITextField!

    @IBOutlet var distanceSlider: RangeSeekSlider!
    
    @IBOutlet var healthProTextField: UITextField!
    
    @IBOutlet var specializationTextField: UITextField!
    
    let orderPickerView = UIPickerView()
    
    let locationPickerView = UIPickerView()
    
    let healthProPickerView = UIPickerView()
    
    let specializationPickerView = UIPickerView()
    
    var orderOptions = Array<Dictionary<String, Any>>()
    
    var locationOptions = Array<Dictionary<String, Any>>()
    
    var healthProOptions = Array<Dictionary<String, Any>>()
    
    var specializationOptions = Array<Dictionary<String, Any>>()
    
    var orderSelected: Int = 0
    
    var locationSelected: Int = 0
    
    var healthProSelected: Int = 0
    
    var specializationSelected: Int = 0
    
    var disMin: Float = 0.0
    
    var disMax: Float = 0.0
    
    var lat: Float = 0.0
    
    var long: Float = 0.0
    
    var professionalsFilterControllerDelegate: ProfessionalsFilterControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = UIColor(displayP3Red: 6.0 / 255.0, green: 182.0 / 255.0, blue: 216.0 / 255.0, alpha: 1.0)
        
        let orderGesture = UITapGestureRecognizer(target: self, action: #selector(orderTapped))
        
        let locationGesture = UITapGestureRecognizer(target: self, action: #selector(locationTapped))
        
        let healthProGesture = UITapGestureRecognizer(target: self, action: #selector(healthProTapped))
        
        let specializationGesture = UITapGestureRecognizer(target: self, action: #selector(specializationTapped))
        
        orderTextField.addGestureRecognizer(orderGesture)
        
        locationTextField.addGestureRecognizer(locationGesture)
        
        healthProTextField.addGestureRecognizer(healthProGesture)
        
        specializationTextField.addGestureRecognizer(specializationGesture)
        
        orderOptions.append(["id" : 1, "text" : "1/5"])
        
        orderOptions.append(["id" : 2, "text" : "2/5"])
        
        orderOptions.append(["id" : 3, "text" : "3/5"])
        
        orderOptions.append(["id" : 4, "text" : "4/5"])
        
        orderOptions.append(["id" : 5, "text" : "5/5"])
        
        locationOptions.append(["id" : 30, "text" : "30 kms"])
        
        locationOptions.append(["id" : 50, "text" : "50 kms"])
        
        locationOptions.append(["id" : 100, "text" : "100 kms"])
        
        locationOptions.append(["id" : 150, "text" : "150 kms"])
        
        locationOptions.append(["id" : 1000000000000000000, "text" : "150 kms +"])
        
        getSpecialities(){ result in
            
            self.specializationOptions = result
            
        }
        
        getProfessions(){ result in
            
            self.healthProOptions = result
            
        }
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locationManager.startUpdatingLocation()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        self.lat = Float(locValue.latitude)
        
        self.long = Float(locValue.longitude)
        
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        
        professionalsFilterControllerDelegate?.professionalsFilterControllerDelegateMethod(order: String(self.orderSelected), location: String(self.locationSelected), disMin: String(self.disMin), disMax: String(self.disMax), pros: String(self.healthProSelected), spec: String(self.specializationSelected), lat: String(self.lat), long: String(self.long))
        
        dismiss(animated: true, completion: nil)
        
        
        
    }
    
    @IBAction func distanceSliderChanged(_ sender: RangeSeekSlider) {
        
        self.disMin = Float(sender.minValue)
        
        self.disMax = Float(sender.maxValue)
        
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        var count = 0
        
        if pickerView == orderPickerView
        {
            count = orderOptions.count
        }
        else if pickerView == locationPickerView
        {
            count = locationOptions.count
        }
        else if pickerView == healthProPickerView
        {
            count = healthProOptions.count
        }
        else if pickerView == specializationPickerView
        {
            count = specializationOptions.count
        }
        
        return count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        var value = ""
        
        if pickerView == orderPickerView
        {
            value = (orderOptions[row]["text"] as? String)!
        }
        else if pickerView == locationPickerView
        {
            value = (locationOptions[row]["text"] as? String)!
        }
        else if pickerView == healthProPickerView
        {
            value = (healthProOptions[row]["text"] as? String)!
        }
        else if pickerView == specializationPickerView
        {
            value = (specializationOptions[row]["text"] as? String)!
        }
        
        return value
        
    }
    
    @objc func orderTapped() {
        
        orderPickerView.delegate = self
        
        orderPickerView.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.orderDonePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.orderCancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        orderTextField.inputAccessoryView = toolBar
        
        view.addSubview(orderTextField)
        
        orderTextField.inputView = orderPickerView
        
        orderTextField.becomeFirstResponder()
        
    }
    
    @objc func orderDonePicker() {
        
        orderTextField.resignFirstResponder()
        
        self.orderTextField.text = orderOptions[orderPickerView.selectedRow(inComponent: 0)]["text"] as? String
        
        self.orderSelected = (orderOptions[orderPickerView.selectedRow(inComponent: 0)]["id"] as? Int)!
        
    }
    
    @objc func orderCancelPicker() {
        
        orderTextField.resignFirstResponder()
        
    }
    
    @objc func locationTapped() {
     
        locationPickerView.delegate = self
        
        locationPickerView.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.locationDonePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.locationCancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        locationTextField.inputAccessoryView = toolBar
        
        view.addSubview(locationTextField)
        
        locationTextField.inputView = locationPickerView
        
        locationTextField.becomeFirstResponder()
        
    }
    
    @objc func locationDonePicker() {
        
        locationTextField.resignFirstResponder()
        
        self.locationTextField.text = locationOptions[locationPickerView.selectedRow(inComponent: 0)]["text"] as? String
        
        self.locationSelected = (locationOptions[locationPickerView.selectedRow(inComponent: 0)]["id"] as? Int)!
        
    }
    
    @objc func locationCancelPicker() {
        
        locationTextField.resignFirstResponder()
        
    }
    
    @objc func healthProTapped() {
        
        healthProPickerView.delegate = self
        
        healthProPickerView.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.healthProDonePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.healthProCancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        healthProTextField.inputAccessoryView = toolBar
        
        view.addSubview(healthProTextField)
        
        healthProTextField.inputView = healthProPickerView
        
        healthProTextField.becomeFirstResponder()
        
    }
    
    @objc func healthProDonePicker() {
        
        healthProTextField.resignFirstResponder()
        
        self.healthProTextField.text = healthProOptions[healthProPickerView.selectedRow(inComponent: 0)]["text"] as? String
        
        self.healthProSelected = (healthProOptions[healthProPickerView.selectedRow(inComponent: 0)]["id"] as? Int)!
        
    }
    
    @objc func healthProCancelPicker() {
        
        healthProTextField.resignFirstResponder()
        
    }
    
    @objc func specializationTapped() {
        
        specializationPickerView.delegate = self
        
        specializationPickerView.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.specializationDonePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.specializationCancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        specializationTextField.inputAccessoryView = toolBar
        
        view.addSubview(specializationTextField)
        
        specializationTextField.inputView = specializationPickerView
        
        specializationTextField.becomeFirstResponder()
        
    }
    
    @objc func specializationDonePicker() {
        
        specializationTextField.resignFirstResponder()
        
        self.specializationTextField.text = specializationOptions[specializationPickerView.selectedRow(inComponent: 0)]["text"] as? String
        
        self.specializationSelected = (specializationOptions[specializationPickerView.selectedRow(inComponent: 0)]["id"] as? Int)!
        
    }
    
    @objc func specializationCancelPicker() {
        
        specializationTextField.resignFirstResponder()
        
    }
    
    func getSpecialities(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getSpecialities()
        {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    func getProfessions(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getProfessions()
        {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
}
