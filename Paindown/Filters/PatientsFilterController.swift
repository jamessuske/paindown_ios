//
//  ProfessionalFilterController.swift
//  Paindown
//
//  Created by James Suske on 2019-08-01.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit
import RangeSeekSlider
import CoreLocation

protocol PatientsFilterControllerDelegate: NSObjectProtocol {
    
    func patientsFilterControllerDelegateMethod(order: String, location: String, disMin: String, disMax: String, painMin: String, painMax: String, pain: String, painkillers: String, lat: String, long: String)
    
}

class PatientsFilterController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    
    @IBOutlet var closeButton: UIBarButtonItem!
    
    @IBOutlet var orderTextField: UITextField!
    
    @IBOutlet var locationTextField: UITextField!
    
    
    @IBOutlet var distanceSlider: RangeSeekSlider!
    
    @IBOutlet var painTextField: UITextField!
    
    @IBOutlet var painkillersTextField: UITextField!

    
    @IBOutlet var painYearsSlider: RangeSeekSlider!
    
    let orderPickerView = UIPickerView()
    
    let locationPickerView = UIPickerView()
    
    let painPickerView = UIPickerView()
    
    let painkillersPickerView = UIPickerView()
    
    var orderOptions = Array<Dictionary<String, Any>>()
    
    var locationOptions = Array<Dictionary<String, Any>>()
    
    var painOptions = Array<Dictionary<String, Any>>()
    
    var painkillersOptions = Array<Dictionary<String, Any>>()
    
    var orderSelected: Int = 0
    
    var locationSelected: Int = 0
    
    var painSelected: Int = 0
    
    var painkillersSelected: Int = 0
    
    var disMin: Float = 0.0
    
    var disMax: Float = 0.0
    
    var painMin: Float = 0.0
    
    var painMax: Float = 0.0
    
    var lat: Float = 0.0
    
    var long: Float = 0.0
    
    var patientsFilterControllerDelegate: PatientsFilterControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = UIColor(displayP3Red: 6.0 / 255.0, green: 182.0 / 255.0, blue: 216.0 / 255.0, alpha: 1.0)
        
        let orderGesture = UITapGestureRecognizer(target: self, action: #selector(orderTapped))
        
        let locationGesture = UITapGestureRecognizer(target: self, action: #selector(locationTapped))
        
        let painGesture = UITapGestureRecognizer(target: self, action: #selector(painTapped))
        
        let painkillersGesture = UITapGestureRecognizer(target: self, action: #selector(painkillersTapped))
        
        orderTextField.addGestureRecognizer(orderGesture)
        
        locationTextField.addGestureRecognizer(locationGesture)
        
        painTextField.addGestureRecognizer(painGesture)
        
        painkillersTextField.addGestureRecognizer(painkillersGesture)
        
        orderOptions.append(["id" : 1, "text" : "1/5"])
        
        orderOptions.append(["id" : 2, "text" : "2/5"])
        
        orderOptions.append(["id" : 3, "text" : "3/5"])
        
        orderOptions.append(["id" : 4, "text" : "4/5"])
        
        orderOptions.append(["id" : 5, "text" : "5/5"])
        
        locationOptions.append(["id" : 30, "text" : "30 kms"])
        
        locationOptions.append(["id" : 50, "text" : "50 kms"])
        
        locationOptions.append(["id" : 100, "text" : "100 kms"])
        
        locationOptions.append(["id" : 150, "text" : "150 kms"])
        
        locationOptions.append(["id" : 1000000000000000000, "text" : "150 kms +"])
        
        painkillersOptions.append(["id" : 1, "text" : "Yes"])
        
        painkillersOptions.append(["id" : 2, "text" : "No"])
        
        getPainTypes(){ result in
            
            self.painOptions = result
            
        }
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locationManager.startUpdatingLocation()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        self.lat = Float(locValue.latitude)
        
        self.long = Float(locValue.longitude)
        
    }

    @IBAction func closeButtonPressed(_ sender: Any) {
        
        patientsFilterControllerDelegate.patientsFilterControllerDelegateMethod(order: String(self.orderSelected), location: String(self.locationSelected), disMin: String(self.disMin), disMax: String(self.disMax), painMin: String(self.painMin), painMax: String(self.painMax), pain: String(self.painSelected), painkillers: String(self.painkillersSelected), lat: String(self.lat), long: String(self.long))
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func distanceSliderChanged(_ sender: RangeSeekSlider) {
        
        self.disMin = Float(sender.minValue)
        
        self.disMax = Float(sender.maxValue)
        
    }
    
    @IBAction func painYearsSliderChanged(_ sender: RangeSeekSlider) {
        
        self.painMin = Float(sender.minValue)
        
        self.painMax = Float(sender.maxValue)
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        var count = 0
        
        if pickerView == orderPickerView
        {
            count = orderOptions.count
        }
        else if pickerView == locationPickerView
        {
            count = locationOptions.count
        }
        else if pickerView == painPickerView
        {
            count = painOptions.count
        }
        else if pickerView == painkillersPickerView
        {
            count = painkillersOptions.count
        }
        
        return count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        var value = ""
        
        if pickerView == orderPickerView
        {
            value = (orderOptions[row]["text"] as? String)!
        }
        else if pickerView == locationPickerView
        {
            value = (locationOptions[row]["text"] as? String)!
        }
        else if pickerView == painPickerView
        {
            value = (painOptions[row]["name"] as? String)!
        }
        else if pickerView == painkillersPickerView
        {
            value = (painkillersOptions[row]["text"] as? String)!
        }
        
        return value
        
    }
    
    @objc func orderTapped() {
        
        orderPickerView.delegate = self
        
        orderPickerView.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.orderDonePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.orderCancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        orderTextField.inputAccessoryView = toolBar
        
        view.addSubview(orderTextField)
        
        orderTextField.inputView = orderPickerView
        
        orderTextField.becomeFirstResponder()
        
    }
    
    @objc func orderDonePicker() {
        
        orderTextField.resignFirstResponder()
        
        self.orderTextField.text = orderOptions[orderPickerView.selectedRow(inComponent: 0)]["text"] as? String
        
        self.orderSelected = (orderOptions[orderPickerView.selectedRow(inComponent: 0)]["id"] as? Int)!
        
    }
    
    @objc func orderCancelPicker() {
        
        orderTextField.resignFirstResponder()
        
    }
    
    @objc func locationTapped() {
        
        locationPickerView.delegate = self
        
        locationPickerView.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.locationDonePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.locationCancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        locationTextField.inputAccessoryView = toolBar
        
        view.addSubview(locationTextField)
        
        locationTextField.inputView = locationPickerView
        
        locationTextField.becomeFirstResponder()
        
    }
    
    @objc func locationDonePicker() {
        
        locationTextField.resignFirstResponder()
        
        self.locationTextField.text = locationOptions[locationPickerView.selectedRow(inComponent: 0)]["text"] as? String
        
        self.locationSelected = (locationOptions[locationPickerView.selectedRow(inComponent: 0)]["id"] as? Int)!
        
    }
    
    @objc func locationCancelPicker() {
        
        locationTextField.resignFirstResponder()
        
    }
    
    @objc func painTapped() {
        
        painPickerView.delegate = self
        
        painPickerView.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.painDonePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.painCancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        painTextField.inputAccessoryView = toolBar
        
        view.addSubview(painTextField)
        
        painTextField.inputView = painPickerView
        
        painTextField.becomeFirstResponder()
        
    }
    
    @objc func painDonePicker() {
        
        painTextField.resignFirstResponder()
        
        self.painTextField.text = painOptions[painPickerView.selectedRow(inComponent: 0)]["name"] as? String
        
        self.painSelected = (painOptions[painPickerView.selectedRow(inComponent: 0)]["id"] as? Int)!
        
    }
    
    @objc func painCancelPicker() {
        
        painTextField.resignFirstResponder()
        
    }
    
    @objc func painkillersTapped() {
        
        painkillersPickerView.delegate = self
        
        painkillersPickerView.dataSource = self
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.painkillersDonePicker))
        
        doneButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.painkillersCancelPicker))
        
        cancelButton.tintColor = UIColor(red: 30.0 / 255.0, green: 53.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        painkillersTextField.inputAccessoryView = toolBar
        
        view.addSubview(painkillersTextField)
        
        painkillersTextField.inputView = painkillersPickerView
        
        painkillersTextField.becomeFirstResponder()
        
    }
    
    @objc func painkillersDonePicker() {
        
        painkillersTextField.resignFirstResponder()
        
        self.painkillersTextField.text = painkillersOptions[painkillersPickerView.selectedRow(inComponent: 0)]["text"] as? String
        
        self.painkillersSelected = (painkillersOptions[painkillersPickerView.selectedRow(inComponent: 0)]["id"] as? Int)!
        
    }
    
    @objc func painkillersCancelPicker() {
        
        painkillersTextField.resignFirstResponder()
        
    }
    
    func getPainTypes(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getPainTypes()
        {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
}
