//
//  MenuController.swift
//  Paindown
//
//  Created by James Suske on 2018-07-21.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit

class MenuController: UITableViewController {
    
    var menuItems: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        
        self.tableView.dataSource = self

        menuItems = ["Home", "Healthpros", "Patients", "Blog", "Sign Up", "Login"]
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.tableView.separatorStyle = .none
        
        let imageHeader = UIImageView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 50))
        
        let header = UIView(frame : CGRect(x : 0, y : 0, width : self.tableView.frame.width, height : 200))
        
        imageHeader.image = UIImage(named: "paindown-logo.png")
        
        imageHeader.contentMode = .scaleAspectFit
        
        imageHeader.center = CGPoint(x: header.bounds.midX, y: header.bounds.midY);
        
        header.addSubview(imageHeader)
        
        self.tableView.tableHeaderView = header
        
        if(UserDefaults.standard.bool(forKey: "isLogon") == true)
        {
            menuItems = ["Home", "About Paindown", "Healthpros", "Patients", "Blog", "Messages", "My Account", "Logout"]
        }
        else
        {
            menuItems = ["Home", "About Paindown", "Healthpros", "Patients", "Blog", "Sign Up", "Login"]
        }
        
        tableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath)
        
        cell.textLabel?.textColor = UIColor.white
        
        cell.textLabel?.textAlignment = .center
        
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 18.0)
        
        cell.textLabel?.text = self.menuItems[indexPath.row].localizedUppercase
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if(self.menuItems[indexPath.row] == "Home")
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeController

            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        if(self.menuItems[indexPath.row] == "About Paindown")
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "About") as! AboutController
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        if(self.menuItems[indexPath.row] == "Healthpros")
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "Healthpros") as! ProfessionalsController

            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        if(self.menuItems[indexPath.row] == "Patients")
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "Patients") as! PatientsController

            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        if(self.menuItems[indexPath.row] == "Blog")
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "Blog") as! BlogController
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        if(self.menuItems[indexPath.row] == "Sign Up")
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "Register") as! RegisterController
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        if(self.menuItems[indexPath.row] == "Login")
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "Login") as! LoginController
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        if(self.menuItems[indexPath.row] == "Messages")
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "MessagesSplitView") as! MessagesSplitViewController
            
            present(viewController, animated: true, completion: nil)
        }
        
        if(self.menuItems[indexPath.row] == "My Account")
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "Account") as! AccountController
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        if(self.menuItems[indexPath.row] == "Logout")
        {
            dismiss(animated: true, completion: {
                
                UserDefaults.standard.removeObject(forKey: "email")
                UserDefaults.standard.set(false, forKey: "isLogon")
                
                self.tableView.tableHeaderView = nil
                
            })
        }
        
    }
    
}
