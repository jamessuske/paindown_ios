//
//  WebService.swift
//  Paindown
//
//  Created by James Suske on 2018-07-19.
//  Copyright © 2018 James Suske. All rights reserved.
//

import Foundation
import Alamofire

let webservice = "https://www.paindown.com/paindown_api/public/api/v1/"
let wordpress = "https://www.paindown.com/paindown_blog/wp-json/wp/v2/posts"

class APIController {

    
    func loginUser(email: String, password: String, completion: @escaping (_ result: String) -> Void)
    {
        let parameters: Parameters = [
            "email" : email,
            "password" : password
        ]
        
        Alamofire.request(webservice + "login", method: HTTPMethod.post, parameters: parameters, encoding: URLEncoding.httpBody, headers: [:]).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Dictionary<String, Any>
                    
                    if((jsonData["error"]) != nil)
                    {
                        completion(jsonData["error"] as! String)
                    }
                    else
                    {

                        UserDefaults.standard.set(email, forKey: "email")
                        UserDefaults.standard.set(true, forKey: "isLogon")
                        UserDefaults.standard.set(jsonData["token"], forKey: "token")
                        
                        if(jsonData["pkprofessional"] == nil)
                        {
                            UserDefaults.standard.set("professional", forKey: "type")
                        }
                        else
                        {
                            UserDefaults.standard.set("patient", forKey: "type")
                        }
                        
                        print(jsonData)
                        
                        completion("")
                    }
                    
                }
            }
            else
            {
                completion((response.error?.localizedDescription)!)
            }
            
        }
        
    }
    
    func forgotPassword(email: String, completion: @escaping (_ result: String) -> Void)
    {
        
        let parameters: Parameters = [
            "email": email
        ]
        
        Alamofire.request(webservice + "forgot", method: HTTPMethod.post, parameters: parameters, encoding: URLEncoding.httpBody, headers: [:]).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Dictionary<String, Any>
                    
                    if((jsonData["response"]) == nil)
                    {
                        completion("")
                    }
                    else
                    {
                        completion(jsonData["response"] as! String)
                    }
                    
                }
            }
            else
            {
                completion((response.error?.localizedDescription)!)
            }
            
        }
    }
    
    func getUserProfile(completion: @escaping (_ result: Dictionary<String, Any>) -> Void)
    {
        
        let headers: HTTPHeaders = [
            "Authorization" : "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        ]
        
        Alamofire.request(webservice + "profile", method: HTTPMethod.get, parameters: [:], encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Dictionary<String, Any>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func updatePassword(user: UsersPasswordClass, completion: @escaping (_ result: Dictionary<String, Any>) -> Void)
    {
        
        let parameters: Parameters = [
            "email" : user.email,
            "password" : user.password,
            "newpassword" : user.newpassword,
            "password_confirmation" : user.password_confirmation
        ]
        
        let headers: HTTPHeaders = [
            "Authorization" : "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        ]
        
        Alamofire.request(webservice + "updatePassword", method: HTTPMethod.put, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Dictionary<String, Any>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func updatePerference(user: Array<Dictionary<String, Any>>, completion: @escaping (_ result: Dictionary<String, Any>) -> Void)
    {
        
        let url = URL(string: webservice + "updatePerference")
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "PUT"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.setValue( "Bearer \((UserDefaults.standard.value(forKey: "token") as! String))", forHTTPHeaderField: "Authorization")
        
        request.httpBody = try! JSONSerialization.data(withJSONObject: user)

        Alamofire.request(request).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Dictionary<String, Any>
                    
                    completion(jsonData)
                    
                }
            }
            else
            {
                print(response.error.debugDescription)
            }
            
        }
    }
    
    func getProfessionals(completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        Alamofire.request(webservice + "professionals").responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getProfessionalsFilters(pros: String, spec: String, location: String, order: String, disMin: String, disMax: String, long: String, lat: String, completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {

        Alamofire.request(webservice + "professionalsFilters/" + pros + "/" + spec + "/" + location + "/" + order + "/" + disMin + "/" + disMax + "/" + long + "/" + lat, method: HTTPMethod.get, parameters: [:], encoding: URLEncoding.httpBody, headers: [:]).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            else
            {
                print(response.error.debugDescription)
            }
            
        }
    }
    
    func getPatients(completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        Alamofire.request(webservice + "patients").responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getUserByUsername(username: String, completion: @escaping (_ result: Dictionary<String, Any>) -> Void)
    {
        
        /*let headers: HTTPHeaders = [
            "Authorization" : "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        ]*/
        
        Alamofire.request(webservice + "getUserByUsername" + "/" + username + "/0", method: HTTPMethod.get, parameters: [:], encoding: URLEncoding.httpBody, headers: [:]).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Dictionary<String, Any>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getUserByEmail(email: String, completion: @escaping (_ result: Dictionary<String, Any>) -> Void)
    {
        
        /*let headers: HTTPHeaders = [
            "Authorization" : "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        ]*/
        
        Alamofire.request(webservice + "getUserByEmail" + "/" + email, method: HTTPMethod.get, parameters: [:], encoding: URLEncoding.httpBody, headers: [:]).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Dictionary<String, Any>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getPatientsFilters(painType: String, location: String, order: String, disMin: String, disMax: String, painMin: String, painMax: String, painKillers: String, long: String, lat: String, completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        Alamofire.request(webservice + "patientsFilters/" + painType + "/" + location + "/" + order + "/" + disMin + "/" + disMax + "/" + painMin + "/" + painMax + "/" + painKillers + "/" + long + "/" + lat, method: HTTPMethod.get, parameters: [:], encoding: URLEncoding.httpBody, headers: [:]).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getBlog(completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        Alamofire.request(wordpress + "?_embed").responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getBlogPost(postid: String, completion: @escaping (_ result: Dictionary<String, Any>) -> Void)
    {
        
        Alamofire.request(wordpress + "/" + postid + "?_embed").responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Dictionary<String, Any>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getPainTypes(completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        Alamofire.request(webservice + "paintype").responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getPainLevels(completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        Alamofire.request(webservice + "painlevel").responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getPainkillers(completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        Alamofire.request(webservice + "painkiller").responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getProfessions(completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        Alamofire.request(webservice + "profession").responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getQualifications(completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        Alamofire.request(webservice + "qualification").responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getSpecialities(completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        Alamofire.request(webservice + "specialities").responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getCountries(completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        Alamofire.request(webservice + "country/all").responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
        
    }
    
    func getRegions(id: String, completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        Alamofire.request(webservice + "region/" + id).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getCities(id: String, completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        Alamofire.request(webservice + "city/" + id).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getMessages(completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        let headers: HTTPHeaders = [
            "Authorization" : "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        ]
        
        Alamofire.request(webservice + "messages", method: HTTPMethod.get, parameters: [:], encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getDeletedMessages(completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        let headers: HTTPHeaders = [
            "Authorization" : "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        ]
        
        Alamofire.request(webservice + "messages-deleted", method: HTTPMethod.get, parameters: [:], encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getSentMessages(completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        let headers: HTTPHeaders = [
            "Authorization" : "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        ]
        
        Alamofire.request(webservice + "messages-sent", method: HTTPMethod.get, parameters: [:], encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func getThread(id: String, completion: @escaping (_ result: Dictionary<String, Any>) -> Void)
    {
        
        let headers: HTTPHeaders = [
            "Authorization" : "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        ]
        
        Alamofire.request(webservice + "message/" + id, method: HTTPMethod.get, parameters: [:], encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Dictionary<String, Any>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func composeMessage(username: String, completion: @escaping (_ result: Array<Dictionary<String, Any>>) -> Void)
    {
        
        Alamofire.request(webservice + "composeMessage/" + username).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Array<Dictionary<String, Any>>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func postMessage(recipients: String, subject: String, message: String, completion: @escaping (_ result: Dictionary<String, Any>) -> Void)
    {
        
        let parameters: Parameters = [
            "recipients": recipients,
            "subject": subject,
            "message": message
        ]
        
        let headers: HTTPHeaders = [
            "Authorization" : "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        ]
        
        Alamofire.request(webservice + "postMessage", method: HTTPMethod.post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Dictionary<String, Any>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    func updateMessage(thread: Int, body: String, completion: @escaping (_ result: Dictionary<String, Any>) -> Void)
    {
        
        let parameters: Parameters = [
            "id": thread,
            "message": body
        ]
        
        let headers: HTTPHeaders = [
            "Authorization" : "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        ]
        
        Alamofire.request(webservice + "updateMessage", method: HTTPMethod.put, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Dictionary<String, Any>
                    
                    completion(jsonData)
                    
                }
            }
            
        }
    }
    
    
    func patientResgister(_ cellHolder: PatientsRegisterClass, completion: @escaping (_ result: String) -> Void)
    {
        
        var jsonDict = [AnyHashable: Any]()
        
        jsonDict["patient_username"] = cellHolder.user_name
        
        jsonDict["email"] = cellHolder.email
        
        jsonDict["password"] = cellHolder.password
        
        jsonDict["fkcountry"] = cellHolder.fkcountry
        
        jsonDict["fkregion"] = cellHolder.fkregion
        
        jsonDict["fkcity"] = cellHolder.fkcity
        
        jsonDict["patient_address"] = cellHolder.address
        
        jsonDict["fkpaintype"] = cellHolder.fkpaintype
        
        jsonDict["fkpainlevel"] = cellHolder.fkpainlevel
        
        jsonDict["patient_painyears"] = cellHolder.patient_painyears
        
        jsonDict["patient_usagepainkiller"] = cellHolder.painkiller
        
        jsonDict["painkiller"] = cellHolder.painkiller
        
        jsonDict["patient_otherpainkillers"] = cellHolder.other_painkiller
        
        jsonDict["patient_painstorypublic"] = (cellHolder.is_public) ? 1 : 0
        
        jsonDict["patient_painstory"] = cellHolder.patient_painstory
        
        jsonDict["patient_image"] = cellHolder.patient_image
        
        jsonDict["city_name"] = cellHolder.city_name
        
        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: jsonDict, options: .prettyPrinted)
        
        let urlComponents = NSURLComponents(string: webservice + "register");
        
        let url = urlComponents?.url;
        
        var request = URLRequest(url: url!)
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        request.httpMethod = "POST"
        
        request.httpBody = jsonData
        
        URLSession.shared.dataTask(with: request, completionHandler: {
            (data, response, error) in
            
            //If there is an error return false.

            if(error != nil){
                
                //return false
                
                completion(error!.localizedDescription)
                
            }else{
                
                do
                {
                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]

                    if((jsonData!["message"]) == nil)
                    {
                        completion("")
                    }
                    else
                    {
                        completion(jsonData!["message"] as! String)
                    }
                    
                }
                catch
                {
                    completion(error.localizedDescription)
                }
                
            }
            
        }).resume()
    }
    
    func professionalResgister(_ cellHolder: ProfessionalRegisterClass, completion: @escaping (_ result: String) -> Void)
    {
        
        var jsonDict = [AnyHashable: Any]()
        
        jsonDict["professional_name"] = cellHolder.professional_name
        
        jsonDict["professional_username"] = cellHolder.professional_username
        
        jsonDict["email"] = cellHolder.email
        
        jsonDict["password"] = cellHolder.password
        
        jsonDict["fkcountry"] = cellHolder.fkcountry
        
        jsonDict["fkregion"] = cellHolder.fkregion
        
        jsonDict["fkcity"] = cellHolder.fkcity
        
        jsonDict["professional_address"] = cellHolder.professional_address
        
        jsonDict["professional_phone"] = cellHolder.professional_phone
        
        jsonDict["professional_website"] = cellHolder.professional_website
        
        jsonDict["professional_about"] = cellHolder.professional_about
        
        jsonDict["professional_approach"] = cellHolder.professional_approach
        
        jsonDict["professional_stories"] = cellHolder.professional_stories
        
        jsonDict["fkprofession"] = cellHolder.fkprofession
        
        jsonDict["fkqualifications"] = cellHolder.fkqualifications
        
        jsonDict["fkspecialities"] = cellHolder.fkspecialities
        
        jsonDict["professional_image"] = cellHolder.professional_image
        
        jsonDict["city_name"] = cellHolder.city_name
        
        
        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: jsonDict, options: .prettyPrinted)
        
        let urlComponents = NSURLComponents(string: webservice + "registerProfessional");
        
        let url = urlComponents?.url;
        
        var request = URLRequest(url: url!)
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        request.httpMethod = "POST"
        
        request.httpBody = jsonData
        
        URLSession.shared.dataTask(with: request, completionHandler: {
            (data, response, error) in
            
            //If there is an error return false.
            
            if(error != nil){
                
                //return false
                
                completion(error!.localizedDescription)
                
            }else{
                
                do
                {

                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    
                    if((jsonData!["message"]) == nil)
                    {
                        completion("")
                    }
                    else
                    {
                        completion(jsonData!["message"] as! String)
                    }
                    
                }
                catch
                {
                    completion(error.localizedDescription)
                }
                
            }
            
        }).resume()
    }
    
    
    func postReview(problem: Int, text: String, rating: Float, user: String, completion: @escaping (_ result: String) -> Void)
    {
        
        let parameters: Parameters = [
            "problem": problem,
            "text": text,
            "rating": rating,
            "user": user,
        ]
        
        let headers: HTTPHeaders = [
            "Authorization" : "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        ]
        
        Alamofire.request(webservice + "reviews", method: HTTPMethod.post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
            
            if(response.error == nil)
            {
                if let result = response.result.value {
                    
                    let jsonData = result as! Dictionary<String, Any>
                    
                    if((jsonData["message"]) == nil)
                    {
                        completion("")
                    }
                    else
                    {
                        completion(jsonData["message"] as! String)
                    }
                    
                }
            }
            else
            {
                completion((response.error?.localizedDescription)!)
            }
            
        }
    }
    
    func updateProfessionals(cellHolder: UsersProfessionalClass, completion: @escaping (_ result: String) -> Void)
    {
        
        var jsonDict = [AnyHashable: Any]()
        
        jsonDict["about"] = cellHolder.about
        
        jsonDict["approach"] = cellHolder.approach
        
        jsonDict["email"] = cellHolder.email
        
        //jsonDict["image"] = cellHolder.image
        
        //jsonDict["isPainkillers"] = cellHolder.isPainkillers
        
        //jsonDict["location"] = cellHolder.location
        
        //jsonDict["membership"] = cellHolder.membership
        
        //jsonDict["membership_date"] = cellHolder.membership_date
        
        jsonDict["name"] = cellHolder.name
        
        jsonDict["phone"] = cellHolder.phone
        
        jsonDict["preference"] = cellHolder.preference
        
        jsonDict["profession"] = cellHolder.profession
        
        jsonDict["public"] = cellHolder.isPublic
        
        //jsonDict["specialities"] = cellHolder.specialities
        
        //jsonDict["qualifications"] = cellHolder.qualifications
        
        jsonDict["website"] = cellHolder.website
        
        jsonDict["stories"] = cellHolder.stories
        
        jsonDict["user_name"] = cellHolder.user_name
        
        
        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: jsonDict, options: .prettyPrinted)
        
        let urlComponents = NSURLComponents(string: webservice + "user");
        
        let url = urlComponents?.url;
        
        var request = URLRequest(url: url!)
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        request.httpMethod = "PUT"
        
        request.httpBody = jsonData
        
        URLSession.shared.dataTask(with: request, completionHandler: {
            (data, response, error) in
            
            //If there is an error return false.
            
            if(error != nil){
                
                //return false
                
                completion(error!.localizedDescription)
                
            }else{
                
                do
                {
                    
                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]

                    
                    if((jsonData!["message"]) == nil)
                    {
                        completion("")
                    }
                    else
                    {
                        completion(jsonData!["message"] as! String)
                    }
                    
                }
                catch
                {
                    completion(error.localizedDescription)
                }
                
            }
            
        }).resume()
        
    }
    
}
