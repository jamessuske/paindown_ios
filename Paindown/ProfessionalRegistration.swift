//
//  ProfessionalRegistration.swift
//  Paindown
//
//  Created by James Suske on 2019-04-14.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class ProfessionalRegistration: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource, ProfessionalsUserDelegate, ProfessionalsLocationDelegate, ProfessionalsProfessionDelegate, ProfessionalsAboutDelegate, ProfessionalsApproachDelegate, ProfessionalsStoryDelegate, ProfessionalsSpecialitiesDelegate, ProfessionalsQualificationsDelegate {
    
    var professional = ProfessionalRegisterClass()
    
    var indicator: UIActivityIndicatorView!
    
    lazy var orderedViewControllers: [UIViewController] = {
        return [
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfessionalsUserController") as! ProfessionalsUserController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfessionalsLocationController") as! ProfessionalsLocationController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfessionalsProfessionController") as! ProfessionalsProfessionController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfessionalsAboutController") as! ProfessionalsAboutController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfessionalsApproachController") as! ProfessionalsApproachController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfessionalsStoryController") as! ProfessionalsStoryController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfessionalsSpecialitiesController") as! ProfessionalsSpecialitiesController,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfessionalsQualificationsController") as! ProfessionalsQualificationsController,
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        
        let professionalsUserController = orderedViewControllers[0] as! ProfessionalsUserController
        let professionalsLocationController = orderedViewControllers[1] as! ProfessionalsLocationController
        let professionalsProfessionController = orderedViewControllers[2] as! ProfessionalsProfessionController
        let professionalsAboutController = orderedViewControllers[3] as! ProfessionalsAboutController
        let professionalsApproachController = orderedViewControllers[4] as! ProfessionalsApproachController
        let professionalsStoryController = orderedViewControllers[5] as! ProfessionalsStoryController
        let professionalsSpecialitiesController = orderedViewControllers[6] as! ProfessionalsSpecialitiesController
        let professionalsQualificationsController = orderedViewControllers[7] as! ProfessionalsQualificationsController
        
        professionalsUserController.professionalsUserDelegate = self
        professionalsLocationController.professionalsLocationDelegate = self
        professionalsProfessionController.professionalsProfessionDelegate = self
        professionalsAboutController.professionalsAboutDelegate = self
        professionalsApproachController.professionalsApproachDelegate = self
        professionalsStoryController.professionalsStoryDelegate = self
        professionalsSpecialitiesController.professionalsSpecialitiesDelegate = self
        professionalsQualificationsController.professionalsQualificationsDelegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func professionalsUserDelegateMethod(name: String, username: String, phoneNumber: String, email: String, password: String, imageUrl: String) {
        
        professional.professional_name = name
        professional.professional_username = username
        professional.professional_phone = phoneNumber
        professional.email = email
        professional.password = password
        professional.professional_image = imageUrl
        
        setViewControllers([orderedViewControllers[1]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
    }
    
    func professionalsLocationDelegateMethod(fkcountry: Int, fkregion: Int, fkcity: Int, professional_address: String, city_name: String) {
        
        
        professional.fkcountry = fkcountry
        professional.fkregion = fkregion
        professional.fkcity = fkcity
        professional.professional_address = professional_address
        professional.city_name = city_name
        
        setViewControllers([orderedViewControllers[2]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
    }
    
    func professionalsProfessionDelegateMethod(fkprofession: String, professional_website: String) {
        
        professional.fkprofession = fkprofession
        professional.professional_website = professional_website
        
        setViewControllers([orderedViewControllers[3]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
    }
    
    func professionalsAboutDelegateMethod(professional_about: String) {
        
        professional.professional_about = professional_about
        
        setViewControllers([orderedViewControllers[4]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
    }
    
    func professionalsApproachDelegateMethod(professional_approach: String) {
        
        professional.professional_approach = professional_approach
        
        setViewControllers([orderedViewControllers[5]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
    }
    
    func professionalsStoryDelegateMethod(professional_stories: String) {
        
        professional.professional_stories = professional_stories
        
        setViewControllers([orderedViewControllers[6]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
    }
    
    func professionalsSpecialitiesDelegateMethod(fkspecialities: Array<Dictionary<String, Any>>) {
        
        self.professional.fkspecialities = fkspecialities
        
        setViewControllers([orderedViewControllers[7]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
    }
    
    func professionalsQualificationsDelegateMethod(fkqualifications: Array<Dictionary<String, Any>>) {
        
        self.professional.fkqualifications = fkqualifications
        
        self.createIndicator()
        
        self.professionalRegister(professional: self.professional){ result in
            
            if(result == "")
            {
                self.customAlert(title: "Error", message: "There was an issue with the registration, please try again later.")
            }
            else
            {
                self.customAlert(title: "Success", message: result)
            }
            
            self.stopIndicator()
            
        }
        
    }
    
    func patientsLocationDelegateBackMethod() {
        
        setViewControllers([orderedViewControllers[0]],
                           direction: .reverse,
                           animated: true,
                           completion: nil)
        
    }
    
    func professionalsProfessionDelegateBackMethod() {
        
        setViewControllers([orderedViewControllers[1]],
                           direction: .reverse,
                           animated: true,
                           completion: nil)
        
    }
    
    func professionalsAboutDelegateBackMethod() {
        
        setViewControllers([orderedViewControllers[2]],
                           direction: .reverse,
                           animated: true,
                           completion: nil)
        
    }
    
    func professionalsApproachDelegateBackMethod() {
        
        setViewControllers([orderedViewControllers[3]],
                           direction: .reverse,
                           animated: true,
                           completion: nil)
        
    }
    
    func professionalsStoryDelegateBackMethod() {
        
        setViewControllers([orderedViewControllers[4]],
                           direction: .reverse,
                           animated: true,
                           completion: nil)
        
    }
    
    func professionalsSpecialitiesDelegateBackMethod() {
        
        setViewControllers([orderedViewControllers[5]],
                           direction: .reverse,
                           animated: true,
                           completion: nil)
        
    }
    
    func professionalsQualificationsDelegateBackMethod() {
        
        setViewControllers([orderedViewControllers[6]],
                           direction: .reverse,
                           animated: true,
                           completion: nil)
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return orderedViewControllers.last
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
            return orderedViewControllers.first
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
        
    }
    
    func professionalRegister(professional: ProfessionalRegisterClass, completionHandler:@escaping (_ result:String) -> Void)
    {
        var returnedResults = String()
        
        APIController().professionalResgister(professional)
        {
            (result: String) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
    func createIndicator()
    {
        
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100)) as UIActivityIndicatorView
        
        indicator.center = self.view.center
        
        indicator.hidesWhenStopped = true
        
        indicator.style = UIActivityIndicatorView.Style.white
        
        indicator.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        
        indicator.layer.cornerRadius = 15
        
        self.view.addSubview(indicator)
        
        self.indicator.startAnimating()
    }
    
    func stopIndicator()
    {
        DispatchQueue.main.async {
            
            self.indicator.stopAnimating()
            self.indicator.removeFromSuperview()
            
        }
        
    }
    
    func customAlert(title: String, message: String)
    {
        
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
