//
//  HomeCell.swift
//  Paindown
//
//  Created by James Suske on 2019-06-16.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class HomeCell: UICollectionViewCell {
    
    @IBOutlet var icon: UIImageView!
    @IBOutlet var title: UILabel!
    
    
}
