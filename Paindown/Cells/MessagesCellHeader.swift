//
//  MessagesCellHeader.swift
//  Paindown
//
//  Created by James Suske on 2019-07-02.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class MessagesCellHeader: UICollectionReusableView {
    
    @IBOutlet var subject: UILabel!

}
