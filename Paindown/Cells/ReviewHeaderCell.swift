//
//  ReviewHeaderCell.swift
//  Paindown
//
//  Created by James Suske on 2019-06-30.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit
import Cosmos

class ReviewHeaderCell: UICollectionReusableView {
    
    @IBOutlet var totalReviews: UILabel!
    
    @IBOutlet var rating: CosmosView!
    
}
