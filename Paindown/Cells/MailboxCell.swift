//
//  MailboxCell.swift
//  Paindown
//
//  Created by James Suske on 2019-05-18.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class MailboxCell: UITableViewCell {
    
    @IBOutlet weak var cellText: UILabel!
    @IBOutlet var cellSubject: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
