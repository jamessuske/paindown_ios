//
//  ConditionCell.swift
//  Paindown
//
//  Created by James Suske on 2019-07-22.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class ConditionCell: UICollectionViewCell {

    @IBOutlet var conditionType: UILabel!
    
    @IBOutlet var conditionText: UILabel!
    
    
}
