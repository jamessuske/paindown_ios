//
//  BlogCell.swift
//  Paindown
//
//  Created by James Suske on 2018-09-11.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit

class BlogCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellText: UILabel!
    @IBOutlet weak var cellDescription: UILabel!
    
    var postid = String()
    
}

