//
//  ReviewFooterCell.swift
//  Paindown
//
//  Created by James Suske on 2019-06-30.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit
import Cosmos

protocol ReviewFooterDelegate: NSObjectProtocol {
    
    func submitReviewDelegate(text: String, rating: Float)
}

class ReviewFooterCell: UICollectionReusableView {
    
    @IBOutlet var text: UITextView!
    
    @IBOutlet var submitButton: UIButton!
    
    @IBOutlet var rating: CosmosView!
    
    var reviewFooterDelegate: ReviewFooterDelegate!
    
    @IBOutlet var problem: UITextField!
    
    @IBAction func submitButtonPressed(_ sender: Any) {
     
        reviewFooterDelegate.submitReviewDelegate(text: self.text.text, rating: Float(rating.rating))
        
    }
    
}
