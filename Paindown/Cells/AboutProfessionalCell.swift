//
//  AboutProfessionalCell.swift
//  Paindown
//
//  Created by James Suske on 2019-07-22.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class AboutProfessionalCell: UICollectionViewCell {
    
    @IBOutlet var aboutLabel: UILabel!
    
    @IBOutlet var aboutText: UITextView!
    
}
