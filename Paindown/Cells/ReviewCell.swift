//
//  ReviewCell.swift
//  Paindown
//
//  Created by James Suske on 2019-06-18.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit
import Cosmos

class ReviewCell: UICollectionViewCell {
    
    @IBOutlet var review: UITextView!
    
    @IBOutlet var published: UILabel!
    
    @IBOutlet var rating: CosmosView!
    
}
