//
//  MessagesCell.swift
//  Paindown
//
//  Created by James Suske on 2019-06-05.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit

class MessagesCell: UICollectionViewCell {
    
    @IBOutlet weak var cellText: UITextView!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var date: UILabel!
    
}
