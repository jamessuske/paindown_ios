//
//  CustomCell.swift
//  Paindown
//
//  Created by James Suske on 2018-07-22.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit

class PatientCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellText: UILabel!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellDescription: UITextView!
    
}
