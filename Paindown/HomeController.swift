//
//  HomeController.swift
//  Paindown
//
//  Created by James Suske on 2018-07-20.
//  Copyright © 2018 James Suske. All rights reserved.
//

import UIKit
import SideMenu

class HomeController: UICollectionViewController {
    
    @IBOutlet var menuButton: UIBarButtonItem!
    
    var homeArray = Array<HomeClass>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        homeArray = []
        
        homeArray.append(HomeClass(icon: "professional", title: "View Professionals", controller: "Healthpros"))
        homeArray.append(HomeClass(icon: "patient", title: "View Patients", controller: "Patients"))
        
        if(UserDefaults.standard.bool(forKey: "isLogon") == true)
        {
            homeArray.append(HomeClass(icon: "my-account", title: "My Account", controller: "Account"))
        }
        else
        {
            homeArray.append(HomeClass(icon: "sign-up", title: "Sign Up", controller: "Register"))
        }
        
        self.collectionView.reloadData()
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    @IBAction func menuButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "MenuController") as! MenuController
        
        let menuRightNavigationController = UISideMenuNavigationController(rootViewController: viewController)
        
        SideMenuManager.default.menuRightNavigationController = menuRightNavigationController

        present(menuRightNavigationController, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.homeArray.count
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeCell
        
        cell.icon.image = UIImage(named: self.homeArray[indexPath.row].icon)
        
        cell.title.text = self.homeArray[indexPath.row].title
        
        return cell
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(self.homeArray[indexPath.row].controller == "Account")
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "Account") as! AccountController
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(self.homeArray[indexPath.row].controller == "Register")
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "Register") as! RegisterController
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(self.homeArray[indexPath.row].controller == "Healthpros")
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "Healthpros") as! ProfessionalsController
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if(self.homeArray[indexPath.row].controller == "Patients")
        {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "Patients") as! PatientsController
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
}
