//
//  ProfessionalMapController.swift
//  Paindown
//
//  Created by James Suske on 2019-04-15.
//  Copyright © 2019 James Suske. All rights reserved.
//

import UIKit
import GoogleMaps
import SideMenu

class ProfessionalMapController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, ProfessionalsFilterControllerDelegate {
    
    let locationManager = CLLocationManager()
    
    @IBOutlet var listButtonPressed: UIBarButtonItem!
    
    @IBOutlet var filterButtonPressed: UIBarButtonItem!
    
    var mapView = GMSMapView()
    
    var professionals = Array<Dictionary<String, Any>>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: 22.300000, longitude: 70.783300, zoom: 10.0)
        mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        mapView.mapType = .terrain
        mapView.camera = camera
        mapView.delegate = self
        self.view = mapView
        
        getProfessionals(){ result in
            
            self.professionals = result
            
            self.locationManager.requestAlwaysAuthorization()
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                self.locationManager.delegate = self
                self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                self.locationManager.startUpdatingLocation()
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        let newcamera = GMSCameraPosition.camera(withLatitude: (locValue.latitude), longitude: (locValue.longitude), zoom: 13.0)

        self.mapView.animate(to: newcamera)
        
        for professional in self.professionals
        {
           
            let city = (professional["location"] as! Dictionary<String, Dictionary<String, Any>>)["city"]!["name"] as! String
            let region = (professional["location"] as! Dictionary<String, Dictionary<String, Any>>)["region"]!["name"] as! String
            let country = (professional["location"] as! Dictionary<String, Dictionary<String, Any>>)["country"]!["name"] as! String
            
            if((professional["position"] as! Dictionary<String, Any>)["lat"]! as! CLLocationDegrees != 0 && (professional["position"] as! Dictionary<String, Any>)["long"]! as! CLLocationDegrees != 0)
            {
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: (professional["position"] as! Dictionary<String, Any>)["lat"]! as! CLLocationDegrees, longitude: (professional["position"] as! Dictionary<String, Any>)["long"]! as! CLLocationDegrees)
                marker.title = professional["username"] as? String
                marker.snippet = (professional["profession_name"] as? String)! + "\n" + city + ", " + region + ", " + country
                marker.map = mapView
            }
            
        }
        
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfessionalProfile") as! ProfessionalProfileController
        
        viewController.username = marker.title!
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func filterButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfessionalsFilterController") as! ProfessionalsFilterController
        
        viewController.professionalsFilterControllerDelegate = self
        
        let menuRightNavigationController = UISideMenuNavigationController(rootViewController: viewController)
        
        SideMenuManager.default.menuRightNavigationController = menuRightNavigationController
        
        present(menuRightNavigationController, animated: true, completion: nil)
        
    }
    
    @IBAction func listButtonPressed(_ sender: Any) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "ProfessionalsController") as! ProfessionalsController
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func professionalsFilterControllerDelegateMethod(order: String, location: String, disMin: String, disMax: String, pros: String, spec: String, lat: String, long: String) {
        
        getProfessionalsFilters(pros: pros, spec: spec, location: location, order: order, disMin: disMin, disMax: disMax, long: long, lat: lat){ result in
            
            self.mapView.clear()
            
            self.professionals = result
            
            for professional in self.professionals
            {
                
                let city = (professional["location"] as! Dictionary<String, Dictionary<String, Any>>)["city"]!["name"] as! String
                let region = (professional["location"] as! Dictionary<String, Dictionary<String, Any>>)["region"]!["name"] as! String
                let country = (professional["location"] as! Dictionary<String, Dictionary<String, Any>>)["country"]!["name"] as! String
                
                if((professional["position"] as! Dictionary<String, Any>)["lat"]! as! CLLocationDegrees != 0 && (professional["position"] as! Dictionary<String, Any>)["long"]! as! CLLocationDegrees != 0)
                {
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2D(latitude: (professional["position"] as! Dictionary<String, Any>)["lat"]! as! CLLocationDegrees, longitude: (professional["position"] as! Dictionary<String, Any>)["long"]! as! CLLocationDegrees)
                    marker.title = professional["username"] as? String
                    marker.snippet = (professional["profession_name"] as? String)! + "\n" + city + ", " + region + ", " + country
                    marker.map = self.mapView
                }
                
            }
            
            
        }
        
    }
    
    
    func getProfessionals(completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getProfessionals()
            {
                (result: Array<Dictionary<String, Any>>) in
                
                DispatchQueue.main.async {
                    
                    //Return our results
                    
                    returnedResults = result
                    completionHandler(returnedResults)
                    
                }
                
        }
    }
    
    func getProfessionalsFilters(pros: String, spec: String, location: String, order: String, disMin: String, disMax: String, long: String, lat: String, completionHandler:@escaping (_ result:Array<Dictionary<String, Any>>) -> Void)
    {
        var returnedResults = Array<Dictionary<String, Any>>()
        
        APIController().getProfessionalsFilters(pros: pros, spec: spec, location: location, order: order, disMin: disMin, disMax: disMax, long: long, lat: lat)
        {
            (result: Array<Dictionary<String, Any>>) in
            
            DispatchQueue.main.async {
                
                //Return our results
                
                returnedResults = result
                completionHandler(returnedResults)
                
            }
            
        }
    }
    
    
}
